// Importing modules
const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const passport = require("passport");
/* const crypto = require("crypto");
const multer = require("multer");
const GridFsStorage = require("multer-gridfs-storage");
const Grid = require("gridfs-stream");
const methodOverride = require("method-override"); */

// Creating the Express App
const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Import routes from the file
const users = require("./backend/routes/api/users.js");
const profile = require("./backend/routes/api/profile.js");

// Declare routes to access
app.use("/api/users", users);
app.use("/api/profile", profile);

// Initialise MongoDB Database
const db = require("./backend/utilities/config/keys.js").mongoURI;

mongoose
  .connect(
    db,
    { useNewUrlParser: true }
  )
  .then(() => console.log("MongoDB Connected"))
  .catch(err => console.log(err));

/* // Init GridFS
let gfs;
conn.once("open", () => {
  // Init Stream
  gfs = Grid(conn.db, mongoose.mongo);
  gfs.collection("uploads");
});
 */
// Create storage engine
/* const storage = new GridFsStorage({
  url: require("./backend/utilities/config/keys").mongoURI,
  file: (req, file) =>
    new Promise((resolve, reject) => {
      crypto.randomBytes(16, (err, buf) => {
        if (err) {
          return reject(err);
        }
        const filename = buf.toString("hex") + path.extname(file.originalname);
        const fileInfo = {
          filename,
          bucketName: "uploads"
        };
        resolve(fileInfo);
      });
    })
});
const upload = multer({ storage }); */

// Initialize Passport
app.use(passport.initialize());
require("./backend/utilities/config/auth")(passport);

// Initialising Port
const port = process.env.PORT || 5000;
app.listen(port, () => console.log(`Server running on port ${port}`));
