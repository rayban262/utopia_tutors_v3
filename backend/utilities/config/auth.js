const JWTStrategy = require("passport-jwt").Strategy;
const ExtractJWT = require("passport-jwt").ExtractJwt;
const mongoose = require("mongoose");

const registerCollection = mongoose.model("register");
const keys = require("./keys");

// Options is an object literal containing options how the token is extracted or verified
const options = {};
options.jwtFromRequest = ExtractJWT.fromAuthHeaderAsBearerToken();
options.secretOrKey = keys.secretOrKey;

module.exports = passport => {
  passport.use(
    // jwt_payload: object literal containing the decoded JWT payload
    // done: passport error first callback accepting arguments done(error, user, info)
    new JWTStrategy(options, (jwt_payload, done) => {
      registerCollection
        .findById(jwt_payload.id)
        .then(user => {
          if (user) {
            return done(null, user);
          }
          return done(null, false);
        })
        .catch(err => console.log(err));
    })
  );
};
