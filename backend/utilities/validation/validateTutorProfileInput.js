const validator = require("validator");
const nullField = require("./supportFunctions/nullChecker").nullField;
const nullObject = require("./supportFunctions/nullChecker").nullObject;
const isEmpty = require("./supportFunctions/isEmpty");
// const isPassword = require("./supportFunctions/isPassword");

module.exports = validateTutorProfileInput = data => {
  const errors = {};
  // Personal Information
  data.personalInformation.name.firstName = nullField(
    data.personalInformation.name.firstName
  );
  data.personalInformation.name.lastName = nullField(
    data.personalInformation.name.lastName
  );
  data.personalInformation.phoneNumber = nullField(
    data.personalInformation.phoneNumber
  );
  data.personalInformation.nric = nullField(data.personalInformation.nric);
  data.personalInformation.gender = nullField(data.personalInformation.gender);
  data.personalInformation.race = nullField(data.personalInformation.race);
  data.personalInformation.dateOfBirth = nullField(
    data.personalInformation.dateOfBirth
  );
  data.personalInformation.postalCode = nullField(
    data.personalInformation.postalCode
  );
  data.personalInformation.bank = nullField(data.personalInformation.bank);
  data.personalInformation.accountNumber = nullField(
    data.personalInformation.accountNumber
  );

  // Tutoring Preferences
  // Null Objects
  data.tutoringPreferences = nullObject(data.tutoringPreferences);
  data.tutoringPreferences.types = nullObject(data.tutoringPreferences.types);
  data.tutoringPreferences.types.preSchool = nullObject(
    data.tutoringPreferences.types.preSchool
  );
  data.tutoringPreferences.types.primary = nullObject(
    data.tutoringPreferences.types.primary
  );
  data.tutoringPreferences.types.lowerSecondary = nullObject(
    data.tutoringPreferences.types.lowerSecondary
  );
  data.tutoringPreferences.types.upperSecondary = nullObject(
    data.tutoringPreferences.types.upperSecondary
  );
  data.tutoringPreferences.types.juniorCollege = nullObject(
    data.tutoringPreferences.types.juniorCollege
  );
  data.tutoringPreferences.types.IB = nullObject(
    data.tutoringPreferences.types.IB
  );
  data.tutoringPreferences.experienceInSpecialNeeds = nullObject(
    data.tutoringPreferences.experienceInSpecialNeeds
  );

  // Null Fields

  data.tutoringPreferences.types.preSchool.subjects = nullField(
    data.tutoringPreferences.types.preSchool.subjects
  );
  data.tutoringPreferences.types.preSchool.minimumRate = nullField(
    data.tutoringPreferences.types.preSchool.minimumRate
  );
  data.tutoringPreferences.types.primary.subjects = nullField(
    data.tutoringPreferences.types.primary.subjects
  );
  data.tutoringPreferences.types.primary.minimumRate = nullField(
    data.tutoringPreferences.types.primary.minimumRate
  );
  data.tutoringPreferences.types.lowerSecondary.subjects = nullField(
    data.tutoringPreferences.types.lowerSecondary.subjects
  );
  data.tutoringPreferences.types.lowerSecondary.minimumRate = nullField(
    data.tutoringPreferences.types.lowerSecondary.minimumRate
  );

  data.tutoringPreferences.types.upperSecondary.subjects = nullField(
    data.tutoringPreferences.types.upperSecondary.subjects
  );
  data.tutoringPreferences.types.upperSecondary.minimumRate = nullField(
    data.tutoringPreferences.types.upperSecondary.minimumRate
  );

  data.tutoringPreferences.types.juniorCollege.subjects = nullField(
    data.tutoringPreferences.types.juniorCollege.subjects
  );
  data.tutoringPreferences.types.juniorCollege.minimumRate = nullField(
    data.tutoringPreferences.types.juniorCollege.minimumRate
  );

  data.tutoringPreferences.types.IB.subjects = nullField(
    data.tutoringPreferences.types.IB.subjects
  );
  data.tutoringPreferences.types.IB.minimumRate = nullField(
    data.tutoringPreferences.types.IB.minimumRate
  );
  data.tutoringPreferences.experienceInSpecialNeeds.specialNeeds = nullField(
    data.tutoringPreferences.experienceInSpecialNeeds.specialNeeds
  );
  data.tutoringPreferences.experienceInSpecialNeeds.experience = nullField(
    data.tutoringPreferences.experienceInSpecialNeeds.experience
  );
  data.tutoringPreferences.preferredTutoringLocations = nullField(
    data.tutoringPreferences.preferredTutoringLocations
  );

  // Teaching Credentials
  // Null Objects
  data.teachingCredentials = nullObject(data.teachingCredentials);
  data.teachingCredentials.academicQualifications = nullObject(
    data.teachingCredentials.academicQualifications
  );
  data.teachingCredentials.academicQualifications.PSLE = nullObject(
    data.teachingCredentials.academicQualifications.PSLE
  );
  data.teachingCredentials.academicQualifications.PSLE.school = nullObject(
    data.teachingCredentials.academicQualifications.PSLE.school
  );
  data.teachingCredentials.academicQualifications.PSLE.subjects = nullObject(
    data.teachingCredentials.academicQualifications.PSLE.subjects
  );
  data.teachingCredentials.academicQualifications.oLevels = nullObject(
    data.teachingCredentials.academicQualifications.oLevels
  );
  data.teachingCredentials.academicQualifications.oLevels.school = nullObject(
    data.teachingCredentials.academicQualifications.oLevels.school
  );
  data.teachingCredentials.academicQualifications.oLevels.subjects = nullObject(
    data.teachingCredentials.academicQualifications.oLevels.subjects
  );
  data.teachingCredentials.academicQualifications.ipYear4 = nullObject(
    data.teachingCredentials.academicQualifications.ipYear4
  );
  data.teachingCredentials.academicQualifications.ipYear4.school = nullObject(
    data.teachingCredentials.academicQualifications.ipYear4.school
  );
  data.teachingCredentials.academicQualifications.ipYear4.subjects = nullObject(
    data.teachingCredentials.academicQualifications.ipYear4.subjects
  );
  data.teachingCredentials.academicQualifications.aLevels = nullObject(
    data.teachingCredentials.academicQualifications.aLevels
  );
  data.teachingCredentials.academicQualifications.aLevels.school = nullObject(
    data.teachingCredentials.academicQualifications.aLevels.school
  );
  data.teachingCredentials.academicQualifications.aLevels.subjects = nullObject(
    data.teachingCredentials.academicQualifications.aLevels.subjects
  );
  data.teachingCredentials.academicQualifications.IB = nullObject(
    data.teachingCredentials.academicQualifications.IB
  );
  data.teachingCredentials.academicQualifications.IB.school = nullObject(
    data.teachingCredentials.academicQualifications.IB.school
  );
  data.teachingCredentials.academicQualifications.IB.subjects = nullObject(
    data.teachingCredentials.academicQualifications.IB.subjects
  );
  data.teachingCredentials.academicQualifications.IGCSE = nullObject(
    data.teachingCredentials.academicQualifications.IGCSE
  );
  data.teachingCredentials.academicQualifications.IGCSE.school = nullObject(
    data.teachingCredentials.academicQualifications.IGCSE.school
  );
  data.teachingCredentials.academicQualifications.IGCSE.subjects = nullObject(
    data.teachingCredentials.academicQualifications.IGCSE.subjects
  );
  data.teachingCredentials.academicQualifications.polyUni = nullObject(
    data.teachingCredentials.academicQualifications.polyUni
  );
  data.teachingCredentials.academicQualifications.polyUni.schools = nullObject(
    data.teachingCredentials.academicQualifications.polyUni.schools
  );

  // Null Fields
  data.teachingCredentials.educationLevel = nullField(
    data.teachingCredentials.educationLevel
  );
  data.teachingCredentials.tutorCategory = nullField(
    data.teachingCredentials.tutorCategory
  );
  data.teachingCredentials.academicQualifications.PSLE.school.name = nullField(
    data.teachingCredentials.academicQualifications.PSLE.school.name
  );
  data.teachingCredentials.academicQualifications.PSLE.school.graduationDate = nullField(
    data.teachingCredentials.academicQualifications.PSLE.school.graduationDate
  );
  data.teachingCredentials.academicQualifications.PSLE.subjects.name = nullField(
    data.teachingCredentials.academicQualifications.PSLE.subjects.name
  );
  data.teachingCredentials.academicQualifications.PSLE.subjects.grade = nullField(
    data.teachingCredentials.academicQualifications.PSLE.subjects.grade
  );
  data.teachingCredentials.academicQualifications.oLevels.school.name = nullField(
    data.teachingCredentials.academicQualifications.oLevels.school.name
  );
  data.teachingCredentials.academicQualifications.oLevels.school.graduationDate = nullField(
    data.teachingCredentials.academicQualifications.oLevels.school
      .graduationDate
  );
  data.teachingCredentials.academicQualifications.oLevels.subjects.name = nullField(
    data.teachingCredentials.academicQualifications.oLevels.subjects.name
  );
  data.teachingCredentials.academicQualifications.oLevels.subjects.grade = nullField(
    data.teachingCredentials.academicQualifications.oLevels.subjects.grade
  );
  data.teachingCredentials.academicQualifications.ipYear4.school.name = nullField(
    data.teachingCredentials.academicQualifications.ipYear4.school.name
  );
  data.teachingCredentials.academicQualifications.ipYear4.school.graduationDate = nullField(
    data.teachingCredentials.academicQualifications.ipYear4.school
      .graduationDate
  );
  data.teachingCredentials.academicQualifications.ipYear4.subjects.name = nullField(
    data.teachingCredentials.academicQualifications.ipYear4.subjects.name
  );
  data.teachingCredentials.academicQualifications.ipYear4.subjects.grade = nullField(
    data.teachingCredentials.academicQualifications.ipYear4.subjects.grade
  );
  data.teachingCredentials.academicQualifications.aLevels.school.name = nullField(
    data.teachingCredentials.academicQualifications.aLevels.school.name
  );
  data.teachingCredentials.academicQualifications.aLevels.school.graduationDate = nullField(
    data.teachingCredentials.academicQualifications.aLevels.school
      .graduationDate
  );
  data.teachingCredentials.academicQualifications.aLevels.subjects.name = nullField(
    data.teachingCredentials.academicQualifications.aLevels.subjects.name
  );
  data.teachingCredentials.academicQualifications.aLevels.subjects.grade = nullField(
    data.teachingCredentials.academicQualifications.aLevels.subjects.grade
  );
  data.teachingCredentials.academicQualifications.IB.school.name = nullField(
    data.teachingCredentials.academicQualifications.IB.school.name
  );
  data.teachingCredentials.academicQualifications.IB.school.graduationDate = nullField(
    data.teachingCredentials.academicQualifications.IB.school.graduationDate
  );
  data.teachingCredentials.academicQualifications.IB.subjects.name = nullField(
    data.teachingCredentials.academicQualifications.IB.subjects.name
  );
  data.teachingCredentials.academicQualifications.IB.subjects.grade = nullField(
    data.teachingCredentials.academicQualifications.IB.subjects.grade
  );
  data.teachingCredentials.academicQualifications.IGCSE.school.name = nullField(
    data.teachingCredentials.academicQualifications.IGCSE.school.name
  );
  data.teachingCredentials.academicQualifications.IGCSE.school.graduationDate = nullField(
    data.teachingCredentials.academicQualifications.IGCSE.school.graduationDate
  );
  data.teachingCredentials.academicQualifications.IGCSE.subjects.name = nullField(
    data.teachingCredentials.academicQualifications.IGCSE.subjects.name
  );
  data.teachingCredentials.academicQualifications.IGCSE.subjects.grade = nullField(
    data.teachingCredentials.academicQualifications.IGCSE.subjects.grade
  );
  data.teachingCredentials.academicQualifications.polyUni.schools.name = nullField(
    data.teachingCredentials.academicQualifications.polyUni.schools.name
  );
  data.teachingCredentials.academicQualifications.polyUni.schools.degreeType = nullField(
    data.teachingCredentials.academicQualifications.polyUni.schools.degreeType
  );
  data.teachingCredentials.academicQualifications.polyUni.schools.course = nullField(
    data.teachingCredentials.academicQualifications.polyUni.schools.course
  );
  data.teachingCredentials.academicQualifications.polyUni.schools.graduationDate = nullField(
    data.teachingCredentials.academicQualifications.polyUni.schools
      .graduationDate
  );
  data.teachingCredentials.experience = nullField(
    data.teachingCredentials.experience
  );

  // isEmpty functions
  if (validator.isEmpty(data.personalInformation.name.firstName)) {
    return (errors.personalInformation.name.firstName =
      "First Name is required");
  }
  if (validator.isEmpty(data.personalInformation.name.lastName)) {
    return (errors.personalInformation.name.lastName = "Last Name is required");
  }
  if (validator.isEmpty(data.personalInformation.nric)) {
    errors.personalInformation.nric = "NRIC number is required";
  }
  if (validator.isEmpty(data.personalInformation.gender)) {
    errors.personalInformation.gender = "Gender is required";
  }
  if (validator.isEmpty(data.personalInformation.race)) {
    errors.personalInformation.race = "Race is required";
  }
  if (validator.isEmpty(data.personalInformation.phoneNumber)) {
    errors.personalInformation.phoneNumber = "Phone number is required";
  }
  if (validator.isEmpty(data.personalInformation.postalCode)) {
    errors.personalInformation.postalCode = "Postal Code is required";
  }
  if (validator.isEmpty(data.personalInformation.bank)) {
    errors.personalInformation.bank = "Bank field is required";
  }
  if (validator.isEmpty(data.personalInformation.accountNumber)) {
    errors.personalInformation.accountNumber = "Account Number is required";
  }

  if (
    validator.isEmpty(data.tutoringPreferences.types.preSchool.minimumRate) &&
    !isEmpty(data.tutoringPreferences.types.preSchool.subjects)
  ) {
    errors.tutoringPreferences.types.preSchool.minimumRate =
      "Please enter your minimum rate";
  }

  if (
    validator.isEmpty(data.tutoringPreferences.types.primary.minimumRate) &&
    !isEmpty(data.tutoringPreferences.types.primary.subjects)
  ) {
    errors.tutoringPreferences.types.primary.minimumRate =
      "Please enter your minimum rate";
  }
  if (
    validator.isEmpty(
      data.tutoringPreferences.types.lowerSecondary.minimumRate
    ) &&
    !isEmpty(data.tutoringPreferences.types.lowerSecondary.subjects)
  ) {
    errors.tutoringPreferences.types.lowerSecondary.minimumRate =
      "Please enter your minimum rate";
  }
  if (
    validator.isEmpty(
      data.tutoringPreferences.types.upperSecondary.minimumRate
    ) &&
    !isEmpty(data.tutoringPreferences.types.upperSecondary.subjects)
  ) {
    errors.tutoringPreferences.types.upperSecondary.minimumRate =
      "Please enter your minimum rate";
  }

  if (
    validator.isEmpty(
      data.tutoringPreferences.types.juniorCollege.minimumRate
    ) &&
    !isEmpty(data.tutoringPreferences.types.juniorCollege.subjects)
  ) {
    errors.tutoringPreferences.types.juniorCollege.minimumRate =
      "Please enter your minimum rate";
  }
  if (
    validator.isEmpty(data.tutoringPreferences.types.IB.minimumRate) &&
    !isEmpty(data.tutoringPreferences.types.IB.subjects)
  ) {
    errors.tutoringPreferences.types.IB.minimumRate =
      "Please enter your minimum rate";
  }
  return {
    errors,
    isValid: isEmpty(errors)
  };
};
