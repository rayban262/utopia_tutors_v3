const validator = require("validator");
const isEmpty = require("./supportFunctions/isEmpty");
// const isPassword = require("./supportFunctions/isPassword");

module.exports = validateRegisterInput = data => {
  const errors = {};
  data.email = !isEmpty(data.email) ? data.email : "";
  data.password = !isEmpty(data.password) ? data.password : "";
  data.password2 = !isEmpty(data.password2) ? data.password2 : "";
  data.tutorOrtutee = !isEmpty(data.tutorOrtutee) ? data.tutorOrtutee : "";

  // isEmpty Functions
  if (validator.isEmpty(data.email)) {
    errors.email = "Email field is required";
  }

  if (validator.isEmpty(data.password)) {
    errors.password = "Password field is required";
  }

  if (validator.isEmpty(data.password2)) {
    errors.password2 = "Please re-enter the password";
  }

  if (
    validator.isEmpty(data.tutorOrtutee) ||
    // Ensures only either tutor or tutee are the provided/selected options
    !(data.tutorOrtutee === "tutor" || data.tutorOrtutee === "tutee")
  ) {
    errors.tutorOrtutee = "Please select a valid option";
  }
  // Data Verifiers

  if (!validator.isEmail(data.email)) {
    errors.email = "Please enter a valid email";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
