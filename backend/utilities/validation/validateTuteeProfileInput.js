const validator = require("validator");
const nullField = require("./supportFunctions/nullChecker").nullField;
const nullObject = require("./supportFunctions/nullChecker").nullObject;
const isEmpty = require("./supportFunctions/isEmpty");
// const isPassword = require("./supportFunctions/isPassword");

module.exports = validateTuteeProfileInput = data => {
  const errors = {};
  // Personal Information
  data.personalInformation.name.firstName = nullField(
    data.personalInformation.name.firstName
  );
  data.personalInformation.name.lastName = nullField(
    data.personalInformation.name.lastName
  );
  data.personalInformation.phoneNumber = nullField(
    data.personalInformation.phoneNumber
  );
  data.personalInformation.gender = nullField(data.personalInformation.gender);
  data.personalInformation.postalCode = nullField(
    data.personalInformation.postalCode
  );

  //Levels Schema
  // Null Objects
  data.levels = nullObject(data.levels);
  data.levels.lowerSecondary = nullObject(data.levels.lowerSecondary);
  data.levels.upperSecondary = nullObject(data.levels.upperSecondary);

  // Null Fields

  data.levels.preSchool = nullField(data.levels.preSchool);
  data.levels.primary = nullField(data.levels.primary);
  data.levels.lowerSecondary.stream = nullField(
    data.levels.lowerSecondary.stream
  );
  data.levels.lowerSecondary.grade = nullField(
    data.levels.lowerSecondary.grade
  );
  data.levels.upperSecondary.stream = nullField(
    data.levels.upperSecondary.stream
  );
  data.levels.upperSecondary.grade = nullField(
    data.levels.upperSecondary.grade
  );
  data.levels.juniorCollege = nullField(data.levels.juniorCollege);
  data.levels.IB = nullField(data.levels.IB);
  data.levels.IGCSE = nullField(data.levels.IGCSE);

  //Subjects Schema
  // Null Objects
  data.subjects = nullObject(data.subjects);
  data.subjects.types = nullObject(data.subjects.types);

  // Null Fields
  data.subjects.types.preSchool = nullField(data.subjects.types.preSchool);
  data.subjects.types.primary = nullField(data.subjects.types.primary);
  data.subjects.types.lowerSecondary = nullField(
    data.subjects.types.lowerSecondary
  );
  data.subjects.types.upperSecondary = nullField(
    data.subjects.types.upperSecondary
  );
  data.subjects.types.juniorCollege = nullField(
    data.subjects.types.juniorCollege
  );
  data.subjects.types.IB = nullField(data.subjects.types.IB);

  // Preferences Schema
  // Null Objects
  data.preferences = nullObject(data.preferences);
  data.preferences.duration = nullObject(data.preferences.duration);

  //Null Fields
  data.preferences.tutorCategory = nullField(data.preferences.tutorCategory);
  data.preferences.duration.hours = nullField(data.preferences.duration.hours);
  data.preferences.duration.frequency = nullField(
    data.preferences.duration.frequency
  );
  data.preferences.tutorCategory.budget = nullField(
    data.preferences.tutorCategory.budget
  );
  data.preferences.tutorCategory.startDate = nullField(
    data.preferences.tutorCategory.startDate
  );
  data.preferences.tutorCategory.shortTermORLongTerm = nullField(
    data.preferences.tutorCategory.shortTermORLongTerm
  );
  data.preferences.tutorCategory.tutorGender = nullField(
    data.preferences.tutorCategory.tutorGender
  );
  data.preferences.tutorCategory.availableTimings = nullField(
    data.preferences.tutorCategory.availableTimings
  );
  data.preferences.tutorCategory.descriptionOfNeeds = nullField(
    data.preferences.tutorCategory.descriptionOfNeeds
  );

  /* 
  if (
    validator.isEmpty(
      data.tutoringPreferences.types.juniorCollege.minimumRate
    ) &&
    !isEmpty(data.tutoringPreferences.types.juniorCollege.subjects)
  ) {
    errors.tutoringPreferences.types.juniorCollege.minimumRate =
      "Please enter your minimum rate";
  }
  if (
    validator.isEmpty(data.tutoringPreferences.types.IB.minimumRate) &&
    !isEmpty(data.tutoringPreferences.types.IB.subjects)
  ) {
    errors.tutoringPreferences.types.IB.minimumRate =
      "Please enter your minimum rate";
  } */
  return {
    errors,
    isValid: isEmpty(errors)
  };
};
