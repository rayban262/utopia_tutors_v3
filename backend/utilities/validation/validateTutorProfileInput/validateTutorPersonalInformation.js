const validator = require("validator");
const nullField = require("../supportFunctions/nullChecker").nullField;
const nullObject = require("../supportFunctions/nullChecker").nullObject;
const isEmpty = require("../supportFunctions/isEmpty");

module.exports = validateTutorPersonalInformation = data => {
  const errors = {};
  // Personal Information
  data.personalInformation_name_firstName = nullField(
    data.personalInformation_name_firstName
  );
  data.personalInformation_name_lastName = nullField(
    data.personalInformation_name_lastName
  );
  data.personalInformation_phoneNumber = nullField(
    data.personalInformation_phoneNumber
  );
  data.personalInformation_nric = nullField(data.personalInformation_nric);
  data.personalInformation_gender = nullField(data.personalInformation_gender);
  data.personalInformation_race = nullField(data.personalInformation_race);
  data.personalInformation_dateOfBirth = nullField(
    data.personalInformation_dateOfBirth
  );
  data.personalInformation_postalCode = nullField(
    data.personalInformation_postalCode
  );
  data.personalInformation_bank = nullField(data.personalInformation_bank);
  data.personalInformation_accountNumber = nullField(
    data.personalInformation_accountNumber
  );

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
