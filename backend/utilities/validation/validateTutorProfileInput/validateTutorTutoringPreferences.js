const validator = require("validator");
const nullField = require("../supportFunctions/nullChecker").nullField;
const nullObject = require("../supportFunctions/nullChecker").nullObject;
const nullFieldBool = require("../supportFunctions/nullChecker").nullFieldBool;
const isEmpty = require("../supportFunctions/isEmpty");

module.exports = validateTutorTutoringPreferences = data => {
  const errors = {};
  // Personal Information
  data.tutoringPreferences_types_preSchool_subjects = nullField(
    data.tutoringPreferences_types_preSchool_subjects
  );
  data.tutoringPreferences_types_preSchool_minimumRate = nullField(
    data.tutoringPreferences_types_preSchool_minimumRate
  );
  data.tutoringPreferences_types_primary_subjects = nullField(
    data.tutoringPreferences_types_primary_subjects
  );
  data.tutoringPreferences_types_primary_minimumRate = nullField(
    data.tutoringPreferences_types_primary_minimumRate
  );
  data.tutoringPreferences_types_lowerSecondary_subjects = nullField(
    data.tutoringPreferences_types_lowerSecondary_subjects
  );
  data.tutoringPreferences_types_lowerSecondary_minimumRate = nullField(
    data.tutoringPreferences_types_lowerSecondary_minimumRate
  );
  data.tutoringPreferences_types_lowerSecondary_teachIP = nullFieldBool(
    data.tutoringPreferences_types_lowerSecondary_teachIP
  );
  data.tutoringPreferences_types_upperSecondary_subjects = nullField(
    data.tutoringPreferences_types_upperSecondary_subjects
  );
  data.tutoringPreferences_types_upperSecondary_minimumRate = nullField(
    data.tutoringPreferences_types_upperSecondary_minimumRate
  );
  data.tutoringPreferences_types_upperSecondary_teachIP = nullFieldBool(
    data.tutoringPreferences_types_upperSecondary_teachIP
  );
  data.tutoringPreferences_types_juniorCollege_subjects = nullField(
    data.tutoringPreferences_types_juniorCollege_subjects
  );
  data.tutoringPreferences_types_juniorCollege_minimumRate = nullField(
    data.tutoringPreferences_types_juniorCollege_minimumRate
  );
  data.tutoringPreferences_types_juniorCollege_teachIP = nullFieldBool(
    data.tutoringPreferences_types_juniorCollege_teachIP
  );
  data.tutoringPreferences_types_IB_subjects = nullField(
    data.tutoringPreferences_types_IB_subjects
  );
  data.tutoringPreferences_types_IB_minimumRate = nullField(
    data.tutoringPreferences_types_IB_minimumRate
  );
  data.tutoringPreferences_experienceInSpecialNeeds_specialNeeds = nullField(
    data.tutoringPreferences_experienceInSpecialNeeds_specialNeeds
  );
  data.tutoringPreferences_experienceInSpecialNeeds_experience = nullField(
    data.tutoringPreferences_experienceInSpecialNeeds_experience
  );
  data.tutoringPreferences_preferredTutoringLocations = nullField(
    data.tutoringPreferences_preferredTutoringLocations
  );

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
