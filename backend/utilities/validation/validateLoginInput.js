const validator = require("validator");
const isEmpty = require("./supportFunctions/isEmpty.js");

module.exports = validateLoginInput = data => {
  const errors = {};
  data.email = !isEmpty(data.email) ? data.email : "";
  data.password = !isEmpty(data.password) ? data.password : "";
  data.tutorOrtutee = !isEmpty(data.tutorOrtutee) ? data.tutorOrtutee : "";

  if (!validator.isEmail(data.email)) {
    errors.email = "Please enter a valid email";
  }

  if (validator.isEmpty(data.email)) {
    errors.email = "Email field is required";
  }

  if (validator.isEmpty(data.password)) {
    errors.password = "Password field is required";
  }

  if (
    validator.isEmpty(data.tutorOrtutee) ||
    // Ensures only either tutor or tutee are the provided/selected options
    !(data.tutorOrtutee === "tutor" || data.tutorOrtutee === "tutee")
  ) {
    errors.tutorOrtutee = "Please select a valid option";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
