const isPassword = pass => {
  const numbers = pass.match(/\d+/g);
  const uppers = pass.match(/[A-Z]/);
  const lowers = pass.match(/[a-z]/);
  const special = pass.match(/[!@#$%\^&*\+]/);

  if (
    numbers === null ||
    uppers === null ||
    lowers === null ||
    special === null
  )
    valid = false;

  if (
    numbers !== null &&
    uppers !== null &&
    lowers !== null &&
    special !== null
  )
    valid = true;

  return valid;
};

module.exports = isPassword;
