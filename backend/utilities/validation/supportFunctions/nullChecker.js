const isEmpty = require("./isEmpty");

// Null value Checker
nullField = field => (!isEmpty(field) ? field : "");

nullObject = field => (!isEmpty(field) ? field : {});

nullFieldBool = field => (!isEmpty(field) ? field : false);

module.exports.nullField = nullField;
module.exports.nullObject = nullObject;
module.exports.nullFieldBool = nullFieldBool;
