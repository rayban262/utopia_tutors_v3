const express = require("express");

const router = express.Router();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

// Validation
const validateRegisterInput = require("../../utilities/validation/validateRegisterInput");
const validateLoginInput = require("../../utilities/validation/validateLoginInput");
// Models
const RegisterModel = require("../../models/Register");
// Key
const keys = require("../../utilities/config/keys");
// @route   GET request to api/users/test
// @desc    Tests users route
// @access  Public
router.get("/test", (req, res) => res.json({ msg: "Users Works" }));

// @route   POST request to api/users/login
// @desc    Login Page
// @access  Private

router.post("/register", (req, res) => {
  // Validate Input
  const { errors, isValid } = validateRegisterInput(req.body);
  if (!isValid) {
    return res.status(400).json(errors);
  }

  // Check for existing
  RegisterModel.findOne({
    email: req.body.email
  }).then(person => {
    if (person) {
      errors.user = "User already exists";
      return res.status(400).json(errors);
    }
    // Register new User
    const registerNew = new RegisterModel({
      email: req.body.email,
      password: req.body.password
    });

    // Generates a salt and a hash for the password
    bcrypt.hash(req.body.password, 10, (err, hash) => {
      if (err) {
        throw err;
      }
      registerNew.password = hash;
      // Saves user to database
      registerNew
        .save()
        .then(user => res.json(user))
        .catch(err => console.log(err));
    });
  });
});

// @route   POST request to api/users/login
// @desc    Login Page
// @access  Private
router.post("/login", (req, res) => {
  // Validate Input
  const { errors, isValid } = validateLoginInput(req.body);
  if (!isValid) {
    return res.status(400).json(errors);
  }
  // Assign request body to constants
  const email = req.body.email;
  const password = req.body.password;

  // Check for existing
  RegisterModel.findOne({ email })
    .then(person => {
      if (!person) {
        errors.user = "User not found";
        return res.status(400).json(errors);
      }

      // Compare hash to password input
      bcrypt
        .compare(password, person.password)
        .then(isMatch => {
          if (!isMatch) {
            errors.password = "Password Incorrect";
            return res.status(400).json(errors);
          }

          const payload = {
            id: person.id,
            email: person.email
          };

          // Signs the payload, authorising login
          jwt.sign(
            payload,
            keys.secretOrKey,
            { expiresIn: 3600 },
            (err, token) => {
              res.json({ success: true, token: `Bearer ${token}` });
            }
          );
        })
        .catch(err => console.log(err));
    })
    .catch(err => console.log(err));
});
module.exports = router;
