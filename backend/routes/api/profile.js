const express = require("express");

const router = express.Router();
const passport = require("passport");
// Validation
const validateTutorProfileInput = require("../../utilities/validation/validateTutorProfileInput");
const validateTuteeProfileInput = require("../../utilities/validation/validateTuteeProfileInput.js");
const validateTutorPersonalInformation = require("../../utilities/validation/validateTutorProfileInput/validateTutorPersonalInformation");
const validateTutorTutoringPreferences = require("../../utilities/validation/validateTutorProfileInput/validateTutorTutoringPreferences");
// Models
const RegisterModel = require("../../models/Register");
const TutorModel = require("../../models/Tutor");
const TuteeModel = require("../../models/Tutee");
// Key
const keys = require("../../utilities/config/keys");

/* Once user registers, he is brought to this page OR when one logs in he can update profile in this page
// @route   POST request to api/profile
// @desc    Create User Profile OR Update User Profile
// @access  Private
*/

router.post(
  "/tutor",
  // passport.authenticate -> Is a authentication middleware for protected routes
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const { errors, isValid } = validateTutorPersonalInformation(req.body);
    // This checks whether validation is unsuccessful
    if (!isValid) {
      return res.status(404).json(errors);
    }
    // Object to create tutor profile
    const tutorProfile = {
      user: req.user.id,
      personalInformation: {
        name: {
          firstName: req.body.personalInformation_name_firstName,
          lastName: req.body.personalInformation_name_lastName
        },
        nric: req.body.personalInformation_nric,
        gender: req.body.personalInformation_gender,
        race: req.body.personalInformation_race,
        dateOfBirth: req.body.personalInformation_dateOfBirth,
        phoneNumber: req.body.personalInformation_phoneNumber,
        postalCode: req.body.personalInformation_postalCode,
        bank: req.body.personalInformation_bank,
        accountNumber: req.body.personalInformation_accountNumber
      },
      tutoringPreferences: {
        types: {
          preSchool: {
            subjects: req.body.tutoringPreferences_types_preSchool_subjects,
            minimumRate:
              req.body.tutoringPreferences_types_preSchool_minimumRate
          },
          primary: {
            subjects: req.body.tutoringPreferences_types_primary_subjects,
            minimumRate: req.body.tutoringPreferences_types_primary_minimumRate
          },
          lowerSecondary: {
            subjects:
              req.body.tutoringPreferences_types_lowerSecondary_subjects,
            minimumRate:
              req.body.tutoringPreferences_types_lowerSecondary_minimumRate,
            teachIP: req.body.tutoringPreferences_types_lowerSecondary_teachIP
          },
          upperSecondary: {
            subjects:
              req.body.tutoringPreferences_types_upperSecondary_subjects,
            minimumRate:
              req.body.tutoringPreferences_types_upperSecondary_minimumRate,
            teachIP: req.body.tutoringPreferences_types_upperSecondary_teachIP
          },
          juniorCollege: {
            subjects: req.body.tutoringPreferences_types_juniorCollege_subjects,
            minimumRate:
              req.body.tutoringPreferences_types_juniorCollege_minimumRate,
            teachIP: req.body.tutoringPreferences_types_juniorCollege_teachIP
          },
          IB: {
            subjects: req.body.tutoringPreferences_types_IB_subjects,
            minimumRate: req.body.tutoringPreferences_types_IB_minimumRate
          }
        },
        experienceInSpecialNeeds: {
          specialNeeds:
            req.body.tutoringPreferences_experienceInSpecialNeeds_specialNeeds,
          experience:
            req.body.tutoringPreferences_experienceInSpecialNeeds_experience
        },
        preferredTutoringLocations:
          req.body.tutoringPreferences_preferredTutoringLocations
      },
      teachingCredentials: {
        educationLevel: req.body.teachingCredentials_educationLevel,
        tutorCategory: req.body.teachingCredentials_tutorCategory,
        academicQualifications: {
          PSLE: {
            school:
              req.body.teachingCredentials_academicQualifications_PSLE_school,
            subjects:
              req.body.teachingCredentials_academicQualifications_PSLE_subjects
          },
          oLevels: {
            school:
              req.body
                .teachingCredentials_academicQualifications_oLevels_school,
            subjects:
              req.body
                .teachingCredentials_academicQualifications_oLevels_subjects
          },
          ipYear4: {
            school:
              req.body
                .teachingCredentials_academicQualifications_ipYear4_school,
            subjects:
              req.body
                .teachingCredentials_academicQualifications_ipYear4_subjects
          },
          aLevels: {
            school:
              req.body
                .teachingCredentials_academicQualifications_aLevels_school,
            subjects:
              req.body
                .teachingCredentials_academicQualifications_aLevels_subjects
          },
          IB: {
            school:
              req.body.teachingCredentials_academicQualifications_IB_school,
            subjects:
              req.body.teachingCredentials_academicQualifications_IB_subjects
          },
          IGCSE: {
            school:
              req.body.teachingCredentials_academicQualifications_IGCSE_school,
            subjects:
              req.body.teachingCredentials_academicQualifications_IGCSE_subjects
          },
          polyUni: {
            schools:
              req.body
                .teachingCredentials_academicQualifications_polyUni_schools
          }
        },
        experience:
          req.body.teachingCredentials_academicQualifications_polyUni_experience
      }
    };
    TutorModel.findOne({ user: req.user.id })
      .then(profile => {
        if (profile) {
          // Update any existing profile
          TutorModel.findOneAndUpdate(
            { user: req.user.id },
            { $set: tutorProfile },
            { new: true }
          )
            .then(profile => res.json(profile))
            .catch(err => res.status(404).json(err));
          // If no profile for the user(including login info)
        } else {
          // Save profile if no existing profile
          new TutorModel(tutorProfile)
            .save()
            .then(profile => res.json(profile))
            .catch(err => res.status(404).json(err));
        }
      })
      .catch(err => res.status(404).json(err));
  }
);

/* Gets the User profile to load it for the user
// @route   GET request to api/profile
// @desc    Get Current User's Profile
// @access  Private
*/

router.get(
  "/",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const errors = {};
    // Finds the matching user by quering with the id
    TutorModel.findOne({ user: req.user.id })
      .populate("user")
      .then(profile => {
        if (!profile) {
          errors.noprofile = "There is no profile for this user";
          return res.status(404).json(errors);
        }
        return res.json(profile);
      })
      .catch(err => {
        res.status(404).json(err);
      });
  }
);

module.exports = router;
