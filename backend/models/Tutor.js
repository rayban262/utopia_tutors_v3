const mongoose = require("mongoose");

const registerCollection = mongoose.model("register");

const Schema = mongoose.Schema;

const personalInformationSchema = new Schema({
  name: {
    firstName: { type: String, required: true },
    lastName: { type: String, required: true }
  },
  nric: { type: String, required: true },
  gender: { type: String, required: true, enum: ["male", "female", ""] },
  race: {
    type: String,
    required: true,
    enum: ["chinese", "indian", "malay", "other", ""]
  },
  dateOfBirth: { type: String, required: true },
  phoneNumber: { type: Number, required: true },
  postalCode: { type: String, required: true },
  bank: { type: String, required: true },
  accountNumber: { type: Number, required: true }
});

const tutoringPreferencesSchema = new Schema({
  types: {
    preSchool: {
      subjects: {
        type: [String],
        enum: [
          "english",
          "math",
          "science",
          "chinese",
          "tamil",
          "malay",
          "hindi",
          "phonics",
          "creativeWriting",
          "art",
          ""
        ]
      },
      minimumRate: Number
    },
    primary: {
      subjects: {
        type: [String],
        enum: [
          "english",
          "math",
          "science",
          "chinese",
          "higherChinese",
          "tamil",
          "higherTamil",
          "malay",
          "higherMalay",
          "hindi",
          "phonics",
          "creativeWriting",
          "art",
          ""
        ]
      },
      minimumRate: Number
    },
    lowerSecondary: {
      subjects: {
        type: [String],
        enum: [
          "english",
          "math",
          "science",
          "chinese",
          "higherChinese",
          "tamil",
          "higherTamil",
          "malay",
          "higherMalay",
          "hindi",
          "history",
          "geography",
          "socialSciences",
          "literature",
          "homeEconomics",
          ""
        ]
      },
      minimumRate: Number,
      teachIP: { type: Boolean, default: true }
    },
    upperSecondary: {
      subjects: {
        type: [String],
        enum: [
          "english",
          "elementaryMath",
          "additionalMath",
          "purePhysics",
          "pureChemistry",
          "pureBiology",
          "combinedSciencePC",
          "combinedScienceBC",
          "combinedSciencePB",
          "chinese",
          "higherChinese",
          "tamil",
          "higherTamil",
          "malay",
          "higherMalay",
          "hindi",
          "pureHistory",
          "pureGeography",
          "combinedHumanitiesHSS",
          "combinedHumanitiesGSS",
          "socialSciences",
          "literature",
          "homeEconomics",
          ""
        ]
      },
      minimumRate: Number,
      teachIP: { type: Boolean, default: true }
    },
    juniorCollege: {
      subjects: {
        type: [String],
        enum: [
          "generalPaper",
          "h1Chinese",
          "h1Math",
          "h2Math",
          "h3Math",
          "h1Physics",
          "h2Physics",
          "h3Physics",
          "h1Chemistry",
          "h2Chemistry",
          "h3Chemistry",
          "h1Biology",
          "h2Biology",
          "h3Biology",
          "h1Economics",
          "h2Economics",
          "h3Economics",
          "h1History",
          "h2History",
          "h3History",
          "h1Geography",
          "h2Geography",
          "h3Geography",
          "h1Literature",
          "h2Literature",
          "h3Literature",
          "h2EnglishLanguageLinguistics",
          "h2KnowledgeEnquiry",
          "h2ChineseLanguageLiterature",
          "h1ChinaStudiesEnglish",
          "h2ChinaStudiesEnglish",
          "h1GeneralStudiesChinese",
          "h2ChinaStudiesChinese",
          "h1Malay",
          "h2Malay",
          "h1Chinese",
          "h2Chinese",
          "h1Tamil",
          "h2Tamil",
          "h2Accounting",
          "h2Computing",
          "h2Music",
          "h2Business",
          "h1ProjectWork",
          ""
        ]
      },
      minimumRate: Number,
      teachIP: { type: Boolean, default: true }
    },
    IB: {
      subjects: {
        type: [String],
        enum: [
          "mathematicsHL",
          "mathematicsSL",
          "furtherMathematicsHL",
          "mathematicalStudiesSL",
          "chemistryHL",
          "chemistrySL",
          "physicsHL",
          "physicsSL",
          "biologyHL",
          "biologySL",
          "historyHL",
          "historySL",
          "businessManagementHL",
          "businessManagementSL",
          "geographyHL",
          "geographySL",
          "economicsHL",
          "economicsSL",
          "designTechnologyHL",
          "designTechnologySL",
          "IGTSHL",
          "IGTSSL",
          "philosophyHL",
          "philosophySL",
          "psychologyHL",
          "psychologySL",
          "englishLangLitHL",
          "englishLangLitSL",
          "englishLitHL",
          "englishLitSL",
          "englishBHL",
          "englishBSL",
          "englishAbInitioSL",
          "chineseLangLitHL",
          "chineseLangLitSL",
          "chineseBHL",
          "chineseBSL",
          "chineseAbInitioSL",
          "malayBHL",
          "malayBSL",
          "malayAbInitioSL",
          "tamilBHL",
          "tamilBSL",
          "tamilAbInitioSL",
          "frenchBHL",
          "frenchBSL",
          "frenchAbInitioSL",
          "spanishBHL",
          "spanishBSL",
          "spanishAbInitioSL",
          "hindiBHL",
          "hindiBSL",
          "hindiAbInitioSL",
          "theoryOfKnowledge",
          "extendedEssay",
          ""
        ]
      },
      minimumRate: Number
    }
  },
  experienceInSpecialNeeds: {
    specialNeeds: {
      type: [String],
      enum: [
        "dyslexia",
        "autism",
        "ADHD",
        "angerManagement",
        "slowLearner",
        "downSyndrome",
        ""
      ]
    },
    experience: String
  },
  preferredTutoringLocations: {
    type: [String],
    enum: [
      "north",
      "northWest",
      "west",
      "central",
      "northEast",
      "east",
      "south",
      ""
    ]
  }
});

const teachingCredentialsSchema = new Schema({
  educationLevel: {
    type: String,
    required: true,
    enum: [
      "nLevel",
      "oLevel",
      "polyStudent",
      "polyGraduate",
      "aLevelStudent",
      "aLevelGraduate",
      "undergraduate",
      "graduate",
      "mastersDegree",
      "PhD",
      ""
    ]
  },
  tutorCategory: {
    type: String,
    required: true,
    enum: [
      "partTimeJC",
      "partTimePoly",
      "partTimeUndergraduate",
      "partTimeGraduate",
      "fullTimeTutor",
      "exMOEPri",
      "exMOESec",
      "exMOEJC",
      "currentMOEPri",
      "currentMOESec",
      "currentMOEJC",
      "nurseryTeacher",
      "kindergartenTeacher",
      "polyTeacher",
      "uniTeacher",
      "IBTeacher",
      "IGCSETeacher",
      "NIETraineePriSec",
      "NIETraineeJC",
      ""
    ]
  },
  academicQualifications: {
    PSLE: {
      school: { name: String, graduationDate: Date },
      subjects: [
        {
          name: String,
          grade: String
        }
      ]
    },
    oLevels: {
      school: { name: String, graduationDate: Date },
      subjects: [
        {
          name: String,
          grade: String
        }
      ]
    },
    ipYear4: {
      school: { name: String, graduationDate: Date },
      subjects: [
        {
          name: String,
          grade: String
        }
      ]
    },
    aLevels: {
      school: { name: String, graduationDate: Date },
      subjects: [
        {
          name: String,
          grade: String
        }
      ]
    },
    IB: {
      school: { name: String, graduationDate: Date },
      subjects: [
        {
          name: String,
          grade: String
        }
      ]
    },
    IGCSE: {
      school: { name: String, graduationDate: Date },
      subjects: [
        {
          name: String,
          grade: String
        }
      ]
    },
    polyUni: {
      schools: [
        {
          name: String,
          degreeType: String,
          course: String,
          graduationDate: Date
        }
      ]
    }
  },
  experience: String
});

const TutorSchema = new Schema({
  user: { type: Schema.Types.ObjectId, ref: "register" },
  personalInformation: personalInformationSchema,
  tutoringPreferences: tutoringPreferencesSchema,
  teachingCredentials: teachingCredentialsSchema
});

module.exports = Tutor = mongoose.model("tutor", TutorSchema);
