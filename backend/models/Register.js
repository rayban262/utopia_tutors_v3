const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const RegisterSchema = new Schema({
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  tutorOrtutee: {
    type: String,
    enum: ["tutor", "tutee", ""],
    required: true
  }
  // Need to add a radio button to choose whether to register as tutor or tutee
});

module.exports = Register = mongoose.model("register", RegisterSchema);
