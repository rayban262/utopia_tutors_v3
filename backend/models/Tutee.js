const mongoose = require("mongoose");

const registerCollection = mongoose.model("register");

const Schema = mongoose.Schema;

const personalInformationSchema = new Schema({
  name: {
    firstName: { type: String, required: true },
    lastName: { type: String, required: true }
  },
  gender: { type: String, required: true, enum: ["male", "female"] },
  dateOfBirth: { type: String, required: true },
  phoneNumber: { type: Number, required: true },
  postalCode: { type: String, required: true }
});

const LevelsSchema = new Schema({
  preSchool: {
    type: String,
    enum: ["Nursery", "K1", "K2", ""]
  },
  primary: {
    type: "String",
    enum: ["P1", "P2", "P3", "P4", "P5", "P6", ""]
  },
  lowerSecondary: {
    stream: {
      type: String,
      enum: ["Express", "Normal Academic", "Normal Technical", "IP", ""]
    },
    grade: {
      type: String,
      enum: ["Sec 1", "Sec 2", ""]
    }
  },
  upperSecondary: {
    stream: {
      type: String,
      enum: ["Express", "Normal Academic", "Normal Technical", "IP", ""]
    },
    grade: {
      type: String,
      enum: ["Sec 3", "Sec 4", "Sec 5", ""]
    }
  },
  juniorCollege: {
    type: String,
    enum: ["JC1", "JC2", ""]
  },
  IB: {
    type: String,
    enum: [
      "Grade 1",
      "Grade 2",
      "Grade 3",
      "Grade 4",
      "Grade 5",
      "Grade 6",
      "Grade 7",
      "Grade 8",
      "Grade 9",
      "Grade 10",
      "Grade 11",
      "Grade 12",
      ""
    ]
  },
  IGCSE: {
    type: String,
    enum: ["Grade 7", "Grade 8", "Grade 9", "Grade 10", ""]
  }
});

const SubjectsSchema = new Schema({
  preSchool: {
    type: [String],
    enum: [
      "english",
      "math",
      "science",
      "chinese",
      "tamil",
      "malay",
      "hindi",
      "phonics",
      "creativeWriting",
      "art",
      ""
    ]
  },
  primary: {
    type: [String],
    enum: [
      "english",
      "math",
      "science",
      "chinese",
      "higherChinese",
      "tamil",
      "higherTamil",
      "malay",
      "higherMalay",
      "hindi",
      "phonics",
      "creativeWriting",
      "art",
      ""
    ]
  },
  lowerSecondary: {
    type: [String],
    enum: [
      "english",
      "math",
      "science",
      "chinese",
      "higherChinese",
      "tamil",
      "higherTamil",
      "malay",
      "higherMalay",
      "hindi",
      "history",
      "geography",
      "socialSciences",
      "literature",
      "homeEconomics",
      ""
    ]
  },
  upperSecondary: {
    type: [String],
    enum: [
      "english",
      "elementaryMath",
      "additionalMath",
      "purePhysics",
      "pureChemistry",
      "pureBiology",
      "combinedSciencePC",
      "combinedScienceBC",
      "combinedSciencePB",
      "chinese",
      "higherChinese",
      "tamil",
      "higherTamil",
      "malay",
      "higherMalay",
      "hindi",
      "pureHistory",
      "pureGeography",
      "combinedHumanitiesHSS",
      "combinedHumanitiesGSS",
      "socialSciences",
      "literature",
      "homeEconomics",
      ""
    ]
  },
  juniorCollege: {
    type: [String],
    enum: [
      "generalPaper",
      "h1Chinese",
      "h1Math",
      "h2Math",
      "h3Math",
      "h1Physics",
      "h2Physics",
      "h3Physics",
      "h1Chemistry",
      "h2Chemistry",
      "h3Chemistry",
      "h1Biology",
      "h2Biology",
      "h3Biology",
      "h1Economics",
      "h2Economics",
      "h3Economics",
      "h1History",
      "h2History",
      "h3History",
      "h1Geography",
      "h2Geography",
      "h3Geography",
      "h1Literature",
      "h2Literature",
      "h3Literature",
      "h2EnglishLanguageLinguistics",
      "h2KnowledgeEnquiry",
      "h2ChineseLanguageLiterature",
      "h1ChinaStudiesEnglish",
      "h2ChinaStudiesEnglish",
      "h1GeneralStudiesChinese",
      "h2ChinaStudiesChinese",
      "h1Malay",
      "h2Malay",
      "h1Chinese",
      "h2Chinese",
      "h1Tamil",
      "h2Tamil",
      "h2Accounting",
      "h2Computing",
      "h2Music",
      "h2Business",
      "h1ProjectWork",
      ""
    ]
  },
  IB: {
    type: [String],
    enum: [
      "mathematicsHL",
      "mathematicsSL",
      "furtherMathematicsHL",
      "mathematicalStudiesSL",
      "chemistryHL",
      "chemistrySL",
      "physicsHL",
      "physicsSL",
      "biologyHL",
      "biologySL",
      "historyHL",
      "historySL",
      "businessManagementHL",
      "businessManagementSL",
      "geographyHL",
      "geographySL",
      "economicsHL",
      "economicsSL",
      "designTechnologyHL",
      "designTechnologySL",
      "IGTSHL",
      "IGTSSL",
      "philosophyHL",
      "philosophySL",
      "psychologyHL",
      "psychologySL",
      "englishLangLitHL",
      "englishLangLitSL",
      "englishLitHL",
      "englishLitSL",
      "englishBHL",
      "englishBSL",
      "englishAbInitioSL",
      "chineseLangLitHL",
      "chineseLangLitSL",
      "chineseBHL",
      "chineseBSL",
      "chineseAbInitioSL",
      "malayBHL",
      "malayBSL",
      "malayAbInitioSL",
      "tamilBHL",
      "tamilBSL",
      "tamilAbInitioSL",
      "frenchBHL",
      "frenchBSL",
      "frenchAbInitioSL",
      "spanishBHL",
      "spanishBSL",
      "spanishAbInitioSL",
      "hindiBHL",
      "hindiBSL",
      "hindiAbInitioSL",
      "theoryOfKnowledge",
      "extendedEssay",
      ""
    ]
  }
});
const PreferencesSchema = new Schema({
  tutorCategory: {
    type: String,
    required: true,
    enum: ["partTime", "fullTime", "Ex/CurrentSchoolTeacher", ""]
  },
  duration: {
    hours: Number, //Duration of Lesson
    frequency: Number // Frequency of lessons in a week
  },
  budget: Number,
  startDate: Date,
  shortTermORLongTerm: {
    type: String,
    enum: ["short", "long", ""]
  },
  tutorGender: {
    type: String,
    enum: ["male", "female", ""]
  },
  availableTimings: String,
  descriptionOfNeeds: String
});

const TuteeSchema = new Schema({
  user: { type: Schema.Types.ObjectId, ref: "register" },
  personalInformation: personalInformationSchema,
  levels: LevelsSchema,
  subjects: SubjectsSchema,
  preferences: PreferencesSchema
});

module.exports = Tutee = mongoose.model("tutee", TuteeSchema);
