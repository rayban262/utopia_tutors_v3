import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Provider } from "react-redux";
import "./App.css";
import jwt_decode from "jwt-decode";
import setAuthToken from "./utilities/setAuthToken";
import { login_user, logout_user } from "./redux/actions/authActions";
import { clear_current_profile } from "./redux/actions/profileActions";
import store from "./store";

import Home from "./Components/Pages/Home/Home";
import Login from "./Components/Pages/Login/Login";
import Register from "./Components/Pages/Register/Register";
import Navbar from "./Components/Elements/Navbar/Navbar";
import TutorProfile from "./Components/Pages/Profiles/TutorProfile/TutorProfile";
import PrivateRoute from "./Components/Elements/PrivateRoute";

const paths = {
  home: "/",
  login: "/login",
  register: "/register",
  dashboard: "/dashboard",
  profile: "/profile"
};

// Check for token
if (localStorage.jwtToken) {
  // Set the auth token header auth
  setAuthToken(localStorage.jwtToken);
  // Decode token and get user info and expiration
  const decoded = jwt_decode(localStorage.jwtToken);
  // Set user and isAuthenticated redux state
  store.dispatch(login_user(decoded));

  // Check for expired token
  const currentTime = Date.now() / 1000;
  if (decoded.exp < currentTime) {
    // Logout user
    store.dispatch(logout_user());
    // Clear current Profile
    store.dispatch(clear_current_profile());
    // Redirect to login
    window.location.href = "/login";
  }
}

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <div className="App">
            <Navbar
              Home={paths.home}
              Login={paths.login}
              Register={paths.register}
            />
            <div>
              <Route exact path={paths.home} component={Home} />
              <Route path={paths.login} component={Login} />
              <Route path={paths.register} component={Register} />
              <Switch>
                <PrivateRoute path={paths.profile} component={TutorProfile} />
              </Switch>
            </div>
          </div>
        </Router>
      </Provider>
    );
  }
}

export default App;
