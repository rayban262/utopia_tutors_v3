import axios from "axios";
import { LOGIN_USER, LOGOUT_USER } from "./types";
import setAuthToken from "../../utilities/setAuthToken";
import jwt_decode from "jwt-decode"; // decodes token to get User data
import { getErrors } from "./errors";

/* Register User
Purpose: Dispatches registered data to backend route
Success: log on console
Failure: dispatch errors and error response data
*/
export const registerUser = registerData => dispatch => {
  axios
    .post("/api/users/register") // POST data to registration backend
    .then(res => console.log(res.data)) // Print response to console
    .catch(err => dispatch(getErrors(err.response.data))); // Dispatch errors
};

/* Login User
Purpose: Dispatches login data to backend route
Success:
    1. Extract token from response
    2. Save token to local storage
    3. Set token to auth header
    4. Decode token to get user data
    5. Use this decoded data to generate LOGIN_USER action
Failure: 
   If unsuccessful: dispatch errors and error response data
*/
export const loginUser = loginData => dispatch => {
  axios
    .post("/api/users/login", loginData) // POST data to Login backend route
    .then(res => {
      const { token } = res.data; // Extract token from response
      localStorage.setItem = ("jwtToken", token); // Assign token to jwtToken and save to local storage
      setAuthToken(token); // Set token to auth header
      const decoded = jwt_decode(token); // Decode token to get user data
      dispatch(login_user(decoded)); // Dispatch LOGIN_USER
    })
    .catch(err => dispatch(getErrors(err.response.data))); // Dispatch errors
};
/* Logout User
Purpose: Logs User out
Method:
    Deletes token from local storage
    Dispatches logout action

*/
export const logoutUser = () => dispatch => {
  // Remove token from local storage
  localStorage.removeItem("jwtToken");
  setAuthToken(false);
  dispatch(logout_user());
};

// Action Creators

export const login_user = payload => {
  return {
    type: LOGIN_USER,
    payload: payload
  };
};

export const logout_user = (payload = {}) => {
  return {
    type: LOGOUT_USER,
    payload: payload
  };
};
