//Error Types
export const GET_ERRORS = "GET_ERRORS";
//Login Types
export const LOGIN_USER = "LOGIN_USER";
export const LOGOUT_USER = "LOGOUT_USER";

// Profile Types
export const GET_TUTOR_PROFILE = "GET_TUTOR_PROFILE";
export const PROFILE_LOADING = "PROFILE_LOADING";
export const ADD_PROFILE_DATA = "ADD_PROFILE_DATA";
export const PROFILE_NOT_FOUND = "PROFILE_NOT_FOUND";
export const CLEAR_CURRENT_PROFILE = "CLEAR_CURRENT_PROFILE";
