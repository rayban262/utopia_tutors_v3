import axios from "axios";
import {
  GET_TUTOR_PROFILE,
  PROFILE_LOADING,
  PROFILE_NOT_FOUND,
  CLEAR_CURRENT_PROFILE,
  ADD_PROFILE_DATA
} from "./types";
import setAuthToken from "../../utilities/setAuthToken";
import jwt_decode from "jwt-decode"; // decodes token to get User data
import { getErrors } from "./errors";

/* Create Profile
Purpose: Posts Profile Data to backend route
Success: Log on Console
Failure: Dispatch Errors and Error Response Data
*/
export const createProfile = profileData => dispatch => {
  axios
    .post("/api/profile/tutor/personalInformation", profileData)
    .then(res => console.log(res.data))
    .catch(err => dispatch(getErrors(err.response.data)));
};

export const getTutorProfile = () => dispatch => {
  dispatch({
    type: PROFILE_LOADING
  });
  axios
    .get("/api/profile/tutor/personalInformation")
    .then(res =>
      dispatch({
        type: GET_TUTOR_PROFILE,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_TUTOR_PROFILE,
        payload: {}
      })
    );
};

// export const clearCurrentProfile =

export const set_profile_loading = () => {
  return {
    type: PROFILE_LOADING
  };
};

// Adds profile data to the profile object before submitting to the database
export const add_profile_data = profileData => dispatch => {
  dispatch({
    type: ADD_PROFILE_DATA,
    payload: profileData
  });
};

export const clear_current_profile = () => {
  return {
    type: CLEAR_CURRENT_PROFILE
  };
};
