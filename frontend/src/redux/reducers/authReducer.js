import isEmpty from "../../utilities/isEmpty";
import { LOGIN_USER, LOGOUT_USER } from "../actions/types";

const initialState = {
  isAuthenticated: false,
  user: {}
};

export default function(state = initialState, action) {
  switch (action.type) {
    case LOGIN_USER:
      return {
        ...state, //Used to deconstruct state
        isAuthenticated: !isEmpty(action.payload), //If payload is empty, user not authenticated
        user: action.payload //assign payload to user
      };
    case LOGOUT_USER:
      return {
        ...state,
        isAuthenticated: !isEmpty(action.payload),
        user: action.payload
      };
    default:
      return state;
  }
}
