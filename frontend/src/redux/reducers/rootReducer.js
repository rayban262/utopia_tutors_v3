import { combineReducers } from "redux";
import authReducer from "./authReducer";
import errorReducer from "./errorReducer";
import profileReducer from "./profileReducer";

export default combineReducers({
  auth: authReducer, // Auth Reducer
  errors: errorReducer, // Error Reducer
  profile: profileReducer // Profile Reducer
});
