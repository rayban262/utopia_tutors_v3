import {
  PROFILE_LOADING,
  CLEAR_CURRENT_PROFILE,
  GET_TUTOR_PROFILE,
  ADD_PROFILE_DATA
} from "../actions/types";

const initialState = {
  profile: null,
  loading: false,
  tempProfile: null
};

export default function(state = initialState, action) {
  switch (action.type) {
    case PROFILE_LOADING:
      return {
        ...state,
        loading: true
      };
    case GET_TUTOR_PROFILE:
      return {
        ...state,
        profile: action.payload,
        loading: false
      };
    case CLEAR_CURRENT_PROFILE:
      return {
        ...state,
        profile: null
      };
    // Add Profile Data to the profile object
    case ADD_PROFILE_DATA:
      return {
        ...state,
        tempProfile: action.payload
      };
    default:
      return state;
  }
}
