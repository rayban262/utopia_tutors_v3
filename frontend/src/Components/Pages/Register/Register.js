import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

// Import Elements
import TextFieldGroup from "../../Elements/FormElements/TextFieldGroup/TextFieldGroup";
import RadioFieldGroup from "../../Elements/FormElements/RadioFieldGroup/RadioFieldGroup";

// Import Redux Actions
import { registerUser } from "../../../redux/actions/authActions";
class Register extends Component {
  constructor() {
    super();
    this.state = {
      email: "",
      password: "",
      password2: "",
      tutorOrtutee: "",
      errors: {}
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    /* if (nextProps.auth.isAuthenticated) {
      this.props.history.push("/dashboard");
    } */
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }

  onSubmit(e) {
    e.preventDefault();
    const registerData = {
      email: this.state.email,
      password: this.state.password,
      password2: this.state.password2,
      tutorOrtutee: this.state.tutorOrtutee
    };
    this.props.registerUser(registerData);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  render() {
    const { errors } = this.state;

    return (
      <div className="col-lg-4 justify-content-center">
        <form onSubmit={this.onSubmit}>
          <TextFieldGroup
            label="Email Address"
            type="email"
            errors={[errors.email, errors.user]}
            placeholder="Enter email"
            name="email"
            value={this.state.email}
            onChange={this.onChange}
          />
          <TextFieldGroup
            label="Password"
            type="password"
            errors={[errors.password]}
            placeholder="Password"
            name="password"
            value={this.state.password}
            onChange={this.onChange}
          />
          <TextFieldGroup
            label="Re-enter your password"
            type="password"
            errors={[errors.password2]}
            placeholder="Re-enter your password"
            name="password2"
            value={this.state.password2}
            onChange={this.onChange}
          />
          <label>Registering as a Tutor or Tutee?</label>
          <div className="form-group">
            <RadioFieldGroup
              name="tutorOrtutee"
              labels={["Tutor", "Tutee"]}
              onChange={this.onChange}
              error={errors.tutorOrtutee}
              values={["tutor", "tutee"]}
            />
          </div>
          <button type="submit" className="btn btn-primary">
            Submit
          </button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

Register.propTypes = {
  registerUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

export default connect(
  mapStateToProps,
  { registerUser }
)(Register);
