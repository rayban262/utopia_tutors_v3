import React from "react";

const FAQ = props => {
  return (
    <div id="FAQs">
      <h2 className="text-center">FAQs</h2>
      <h3 id="For-Tutees">For Tutees</h3>
      <dd>
        <dt>
          <h4>Are the Tutors provided by Utopia Reliable?</h4>
        </dt>
        <dl>
          <p>
            Tutors whom register with us are required to submit their
            certifications, and are manually screened by our coordinators to
            ensure subject proficiency (minimum A) and ability to meet tutee’s
            needs. If for whatever reason you are not satisfied with the
            recommended tutor, our friendly coordinators will find a replacement
            for you. Do note that you are still required to make payment for the
            lessons conducted before the change.
          </p>
        </dl>
        <h4>What are the agency fees?</h4>
        <dl>The fees are ....</dl>
      </dd>
      <h3 id="For-Tutors">For Tutors</h3>
      <dd>
        <dt>
          <p>Low Commission Fees</p>
        </dt>
        <dl>
          At Utopia, we understand that tuition agency fees can be hard to
          swallow initially. 50% of your first month’s salary seems too much to
          ask. As such, we have decided to charge a one-of-a-kind, flat
          commission of 3 hours’ worth of lessons. This means that you can give
          as many lessons as you want, and only pay for the first 3 hours!
        </dl>
        <dt>Flexible Schedule and Convenient Locations</dt>
        <dl>
          We match you with students of similar schedules, who live in your
          area. This way, you can meet your earnings objective with ease.
          Travelling time and fees will no longer be an issue.
        </dl>
        <dt>Cashback</dt>
        <dl>
          Utopia’s Tutor of the Week Challenge aims to recognize and reward
          excellent tutors for their dedication. Every week we will choose one
          tutor who has shown passion and professionalism in their teaching. He
          or she will receive a $5 NTUC voucher for free! How’s that for doing
          what you are already paid to do! If you feel that you deserve this
          award, get your student to nominate you here today.
        </dl>
      </dd>
    </div>
  );
};

export default FAQ;
