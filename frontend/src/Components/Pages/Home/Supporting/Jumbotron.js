import React from "react";

const Jumbotron = props => {
  return (
    <div>
      <div className="jumbotron">
        <h1 className="display-4">
          A One stop place for all your tuition needs
        </h1>
        <p className="lead">
          We have tutors that match your needs and deliver results
        </p>
      </div>
    </div>
  );
};

export default Jumbotron;
