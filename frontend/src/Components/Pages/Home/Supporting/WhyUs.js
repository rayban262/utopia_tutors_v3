import React from "react";

/* const cardStyle = {
  width: "18rem",
  height: "10rem"
}; */
const WhyUs = props => {
  return (
    <div id="Why-Us">
      <h2 className="text-center">Why Us?</h2>
      <div className="container-fluid">
        <div className="row">
          <div className="container col-lg-6" />
          <div className="col-md-6">
            <div className="card Why-Us-Card" /*style={cardStyle}*/>
              <div class="card-body">
                <h3 class="card-title text-center">Tutor for every need</h3>
                <div class="card-text text-justify">
                  <p>
                    You will gain access to Utopia’s exclusive pool of tutors
                    where tutors are individually screened to ensure proficiency
                    in the subject they teach (minimum A grade scored in
                    national or university exams). We do so by requiring the
                    submission of certifications and a write up of teaching
                    experiences/testimonials when these tutors register with us.
                  </p>
                  <p>
                    In addition to offering you a tutor who can increase your
                    child’s understanding of fundamental concepts (which we’ve
                    shown to be critical), we strive to provide you with one
                    best suited to your other requirements.
                  </p>
                  <p>Do you prefer tutors of a particular gender?</p>
                  <p>
                    Looking for someone who is willing to teach at a certain
                    hourly rate?
                  </p>
                  <p>
                    Or perhaps you need a very patient tutor in light of your
                    child’s learning difficulties…
                  </p>

                  <p>
                    <strong>We’ve got you covered!</strong>
                  </p>
                  <p>
                    Our tuition coordinators will filter through our large
                    database of qualified tutors and recommend a few of our top
                    tutors to you based on your child’s needs.{" "}
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-6">
            <div className="card Why-Us-Card" /*style={cardStyle}*/>
              <div class="card-body">
                <h3 class="card-title text-center">Quality Tutors</h3>
                <p class="card-text text-justify">
                  Our tutors are individually screened to ensure proficiency in
                  the subject they teach (minimum A grade scored in national
                  exams). At Utopia, tutors’ satisfaction is our priority. We
                  believe that happy tutors are motivated tutors. Our “Tutor of
                  the week” challenge serve to encourage tutors to give their
                  best.
                </p>
              </div>
            </div>
          </div>
          <div className="container col-md-6" />
          <div className="container col-md-6" />
          <div className="col-md-6">
            <div className="card Why-Us-Card" /*style={cardStyle}*/>
              <div class="card-body">
                <h3 class="card-title text-center">Efficient and Free</h3>
                <p class="card-text text-justify">
                  Our efficient and friendly coordinators will shortlist
                  suitable candidates for you for free! Just fill in the request
                  form and we will find you a suitable tutor within 24 hours.
                </p>
              </div>
            </div>
          </div>
          <div className="col-md-6">
            <div className="card Why-Us-Card" /*style={cardStyle}*/>
              <div class="card-body">
                <h3 class="card-title text-center">Referral Program</h3>
                <p class="card-text text-justify">
                  Satisfied with our free service? Refer your friends today to
                  get a $10 NTUC shopping voucher. For every successful
                  referral*, you will receive a $10 voucher mailed to your home.
                  Help us to help you
                </p>
              </div>
            </div>
          </div>
          <div className="container col-md-6" />
        </div>
      </div>
    </div>
  );
};

export default WhyUs;
