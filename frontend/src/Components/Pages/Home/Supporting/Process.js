import React from "react";
import formIcon from "../../../../assets/Form.svg";
import Bullet1 from "../../../../assets//bullets/Bullet1.svg";
import Bullet2 from "../../../../assets//bullets/Bullet2.svg";
import Bullet3 from "../../../../assets//bullets/Bullet3.svg";
/* const cardStyle = {
  width: "18rem",
  height: "10rem"
};*/
const Process = props => {
  return (
    <div>
      <h2 className="text-center">Requesting a tutor is as simple as ABC!</h2>
      <div className="container-fluid">
        <div className="row">
          <div className="col-sm-12 col-md-4 col-lg-4">
            <div className="card Process-Card">
              <div className="card-body">
                <h3 className="card-title text-center">
                  <img src={Bullet1} className="Icon Bullet" />
                  Complete the Form
                </h3>
                <p className="card-text text-justify">
                  Answer a few simple questions that will allow us to find the
                  best tutors that match your needs
                </p>
              </div>
            </div>
          </div>
          <div className="col-sm-12 col-md-4 col-lg-4">
            <div className="card Process-Card">
              <div className="card-body">
                <h3 className="card-title text-center">
                  <img src={Bullet2} className="Icon Bullet" />
                  Receive your SMS
                </h3>
                <p className="card-text text-justify">
                  You will soon receive an SMS after completing our form that
                  lists tutors that perfectly match your requirements
                </p>
              </div>
            </div>
          </div>
          <div className="col-sm-12 col-md-4 col-lg-4">
            <div className="card Process-Card">
              <div className="card-body">
                <h3 className="card-title text-center">
                  <img src={Bullet3} className="Icon Bullet" />
                  Pick your choice
                </h3>
                <p className="card-text text-justify">
                  Indicate your preferred tutor and reply to our SMS. Your
                  preferred tutor will liase with you immediately
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Process;
