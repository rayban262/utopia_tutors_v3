import React, { Component } from "react";
import Jumbotron from "./Supporting/Jumbotron";
import Process from "./Supporting/Process";
import WhyUs from "./Supporting/WhyUs";
import FAQ from "./Supporting/FAQ";
import "./Home.css";

class Home extends Component {
  render() {
    return (
      <div>
        <Jumbotron />
        <Process />
        <WhyUs />
        <FAQ />
      </div>
    );
  }
}

export default Home;
