import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

// Import Elements
import TextFieldGroup from "../../Elements/FormElements/TextFieldGroup/TextFieldGroup";
import RadioFieldGroup from "../../Elements/FormElements/RadioFieldGroup/RadioFieldGroup";

// Import Redux Actions
import { loginUser } from "../../../redux/actions/authActions";

class Login extends Component {
  constructor() {
    super();
    this.state = {
      email: "",
      password: "",
      tutorOrtutee: "",
      errors: {}
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.auth.isAuthenticated) {
      this.props.history.push("/profile");
    }
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }

  /* onSubmit
Purpose: When submit button pressed, this function helpes submit userData to backend
*/
  onSubmit(e) {
    e.preventDefault();
    const userData = {
      email: this.state.email,
      password: this.state.password,
      tutorOrtutee: this.state.tutorOrtutee
    };
    this.props.loginUser(userData);
  }

  /* onChange(e)
  Purpose: When values change in a field, this function updates the state
  */
  onChange(e) {
    /* if (
      [e.target.type] === "radio" &&
      [e.target.className] === "tutorOrtutee"
    ) {
      this.setState({ tutorOrtutee: [e.target.value] });
    } */

    this.setState({ [e.target.name]: e.target.value });
  }

  render() {
    const { errors } = this.state;

    return (
      <div className="col-lg-4 justify-content-center">
        <form onSubmit={this.onSubmit}>
          <TextFieldGroup
            label="Email Address"
            type="email"
            errors={[errors.email, errors.user]}
            name="email"
            placeholder="Enter email"
            value={this.state.email}
            onChange={this.onChange}
          />

          <TextFieldGroup
            label="Password"
            type="password"
            errors={[errors.password]}
            name="password"
            placeholder="Password"
            value={this.state.password}
            onChange={this.onChange}
          />

          <RadioFieldGroup
            error={errors.tutorOrtutee}
            inline={true}
            labels={["Tutor", "Tutee"]}
            name="tutorOrtutee"
            onChange={this.onChange}
            values={["tutor", "tutee"]}
          />

          <button type="submit" className="btn btn-primary">
            Submit
          </button>
        </form>
      </div>
    );
  }
}

/* mapStateToProps
Purpose: Map Root Reducer to Component
*/
const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

Login.propTypes = {
  loginUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};
export default connect(
  mapStateToProps,
  { loginUser } // Connects loginUser action to component
)(Login);
