import React, { Component } from "react";
import classnames from "classnames";
import PropTypes from "prop-types";
import { connect } from "react-redux";

// Import Components
import PersonalInformation from "./PersonalInformation";
import TutoringPreferences from "./TutoringPreferences";
import TeachingCredentials from "./TeachingCredentials";

// Import Redux Actions
import {
  createProfile,
  getTutorProfile
} from "../../../../redux/actions/profileActions";

class TutorProfile extends Component {
  constructor() {
    super();
    /*
    Rules for Naming State Variables:
    1. State Object names must match the MongoDB model object
    2. Replace the "." in the object path with "_" as state object
       does not permit nesting
    3. errors variable to come at the end
    */
    this.state = {
      // Personal Information
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.ondropDownMultipleSelect = this.ondropDownMultipleSelect.bind(this);
  }

  componentDidMount() {
    this.props.getTutorProfile();
  }
  componentWillReceiveProps(nextProps) {
    /* if (nextProps.auth.isAuthenticated) {
      this.props.history.push("/dashboard");
    } */
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }
  onSubmit(e) {
    e.preventDefault();

    //this.props.createProfile(profileData);
  }
  onChange(e) {
    /* if (
      [e.target.type] === "radio" &&
      [e.target.className] === "tutorOrtutee"
    ) {
      this.setState({ tutorOrtutee: [e.target.value] });
    } */

    this.setState({ [e.target.name]: e.target.value });
  }

  ondropDownMultipleSelect(e) {
    var options = e.target.options;
    var value = [];
    for (var i = 0, l = options.length; i < l; i++) {
      if (options[i].selected) {
        value.push(options[i].value);
      }
    }
    this.setState({ value: value });
  }

  render() {
    const { user } = this.props.auth;
    const { profile, loading } = this.props.profile;
    const { errors } = this.state;

    let dashboardContent;
    // After calling getTutorProfile in ComponentDidMount
    if (profile === null || loading) {
      dashboardContent = <h4>Loading...</h4>;
    } else {
      // Check if logged in user has profile data
      if (Object.keys(profile).length > 0) {
        dashboardContent = <h4>TODO: Display Profile</h4>;
      } else {
        // User is logged in but has no profile
        dashboardContent = (
          <div>
            <p className="lead text-muted">Welcome {user.name}</p>
            <p>You have not yet set up a profile, please add some info</p>
          </div>
        );
      }
    }
    return <div>{dashboardContent}</div>;
  }
}

TutorProfile.propTypes = {
  getTutorProfile: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors,
  profile: state.profile
});

export default connect(
  mapStateToProps,
  { createProfile, getTutorProfile }
)(TutorProfile);
