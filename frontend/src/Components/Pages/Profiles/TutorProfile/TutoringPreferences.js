import React, { Component } from "react";
import classnames from "classnames";
import PropTypes from "prop-types";
import Tabs from "../../../Elements/Tabs/Tabs";
import TabTitle from "../../../Elements/Tabs/TabTitle";
import TabContent, {
  DivTabContent,
  TabContentDiv
} from "../../../Elements/Tabs/TabContent";
// Import Form Elements
import DropdownFieldGroup from "../../../Elements/FormElements/DropdownFieldGroup/DropdownFieldGroup";
import TextFieldGroup from "../../../Elements/FormElements/TextFieldGroup/TextFieldGroup";
import CheckboxFieldGroup from "../../../Elements/FormElements/CheckboxFieldGroup/CheckboxFieldGroup";
import TextAreaFieldGroup from "../../../Elements/FormElements/TextAreaFieldGroup/TextAreaFieldGroup";
// import { createProfile } from "../../../../redux/actions/profileActions";

const TutoringPreferences = ({
  onChange,
  onClick,
  types_preSchool_subjects,
  types_preSchool_minimumRate,
  types_primary_subjects,
  types_primary_minimumRate,
  types_lowerSecondary_subjects,
  types_lowerSecondary_minimumRate,
  types_lowerSecondary_teachIP,
  types_upperSecondary_subjects,
  types_upperSecondary_minimumRate,
  types_upperSecondary_teachIP,
  types_juniorCollege_subjects,
  types_juniorCollege_minimumRate,
  types_juniorCollege_teachIP,
  types_IB_subjects,
  types_IB_minimumRate,
  specialNeeds_specialNeeds,
  specialNeeds_experience,
  preferredTutoringLocations
}) => {
  return (
    <div className="form-group">
      <h2>Tutoring Preferences</h2>
      <div>
        <TabTitle
          tabs={[
            "Pre-School",
            "Primary",
            "Lower Secondary",
            "Upper Secondary",
            "Junior College",
            "IB"
          ]}
          tabID={[
            "preSchool",
            "primary",
            "lowerSecondary",
            "upperSecondary",
            "juniorCollege",
            "IB"
          ]}
          contentID={[
            "preSchool-c",
            "primary-c",
            "lowerSecondary-c",
            "upperSecondary-c",
            "juniorCollege-c",
            "IB-c"
          ]}
        />
        <TabContentDiv>
          <TabContent
            tabID="preSchool"
            contentID="preSchool-c"
            showActive={true}
          >
            <DropdownFieldGroup
              name="tutoringPreferences_types_preSchool_subjects"
              fieldTitle="Subjects"
              options={[
                "English",
                "Mathematics",
                "Science",
                "Chinese",
                "Tamil",
                "Malay",
                "Hindi",
                "Phonics",
                "CreativeWriting",
                "Art"
              ]}
              values={[
                "english",
                "math",
                "science",
                "chinese",
                "tamil",
                "malay",
                "hindi",
                "phonics",
                "creativeWriting",
                "art"
              ]}
              multipleSelect={true}
            />
            <TextFieldGroup
              name="tutoringPreferences_types_preSchool_minimumRate"
              value={types_preSchool_minimumRate}
              label="Minimum Rate"
              type="number"
              placeholder="Minimum Rate"
              onChange={onChange}
            />
          </TabContent>
          <TabContent tabID="primary" contentID="primary-c">
            <DropdownFieldGroup
              name="tutoringPreferences_types_primary_subjects"
              fieldTitle="Subjects"
              options={[
                "English",
                "Mathematics",
                "Science",
                "Chinese",
                "Higher Chinese",
                "Tamil",
                "Higher Tamil",
                "Malay",
                "Higher Malay",
                "Hindi",
                "Phonics",
                "CreativeWriting",
                "Art"
              ]}
              values={[
                "english",
                "math",
                "science",
                "chinese",
                "higherChinese",
                "tamil",
                "higherTamil",
                "malay",
                "higherMalay",
                "hindi",
                "phonics",
                "creativeWriting",
                "art"
              ]}
              multipleSelect={true}
            />
            <TextFieldGroup
              name="tutoringPreferences_types_primary_minimumRate"
              value={types_primary_minimumRate}
              label="Minimum Rate"
              type="number"
              placeholder="Minimum Rate"
              onChange={onChange}
            />
          </TabContent>
          <TabContent tabID="lowerSecondary" contentID="lowerSecondary-c">
            <DropdownFieldGroup
              fieldTitle="Subjects"
              name="tutoringPreferences_types_lowerSecondary_subjects"
              options={[
                "English",
                "Mathematics",
                "Science",
                "Chinese",
                "Higher Chinese",
                "Tamil",
                "Higher Tamil",
                "Malay",
                "Higher Malay",
                "Hindi",
                "History",
                "Geography",
                "Social Sciences",
                "Literature",
                "Home Economics"
              ]}
              values={[
                "english",
                "math",
                "science",
                "chinese",
                "higherChinese",
                "tamil",
                "higherTamil",
                "malay",
                "higherMalay",
                "hindi",
                "history",
                "geography",
                "socialSciences",
                "literature",
                "homeEconomics"
              ]}
              multipleSelect={true}
            />
            <TextFieldGroup
              name="tutoringPreferences_types_lowerSecondary_minimumRate"
              value={types_lowerSecondary_minimumRate}
              label="Minimum Rate"
              type="number"
              placeholder="Minimum Rate"
              onChange={onChange}
            />
            <CheckboxFieldGroup
              label="I can teach IP students"
              name="tutoringPreferences_types_lowerSecondary_teachIP"
              value={types_lowerSecondary_teachIP}
            />
          </TabContent>
          <TabContent tabID="upperSecondary" contentID="upperSecondary-c">
            <DropdownFieldGroup
              fieldTitle="Subjects"
              name="tutoringPreferences_types_upperSecondary_subjects"
              options={[
                "English",
                "Elementary Mathematics",
                "Additional Mathematics",
                "Pure Physics",
                "Pure Chemistry",
                "Pure Biology",
                "Combined Science Physics Chemistry",
                "Combined Science Biology Chemistry",
                "Combined Science Physics Biology",
                "Chinese",
                "Higher Chinese",
                "Tamil",
                "Higher Tamil",
                "Malay",
                "Higher Malay",
                "Hindi",
                "Pure History",
                "Pure Geography",
                "Combined Humanities: History, Social Studies",
                "Combined Humanities: Geography, Social Studies",
                "Social Studies",
                "Literature",
                "Home Economics"
              ]}
              values={[
                "english",
                "elementaryMath",
                "additionalMath",
                "purePhysics",
                "pureChemistry",
                "pureBiology",
                "combinedSciencePC",
                "combinedScienceBC",
                "combinedSciencePB",
                "chinese",
                "higherChinese",
                "tamil",
                "higherTamil",
                "malay",
                "higherMalay",
                "hindi",
                "pureHistory",
                "pureGeography",
                "combinedHumanitiesHSS",
                "combinedHumanitiesGSS",
                "socialSciences",
                "literature",
                "homeEconomics"
              ]}
              multipleSelect={true}
            />
            <TextFieldGroup
              name="tutoringPreferences_types_upperSecondary_minimumRate"
              value={types_upperSecondary_minimumRate}
              label="Minimum Rate"
              type="number"
              placeholder="Minimum Rate"
              onChange={onChange}
            />
            <CheckboxFieldGroup
              label="I can teach IP students"
              value={types_upperSecondary_teachIP}
              name="tutoringPreferences_types_upperSecondary_teachIP"
            />
          </TabContent>
          <TabContent tabID="juniorCollege" contentID="juniorCollege-c">
            <DropdownFieldGroup
              fieldTitle="Subjects"
              name="tutoringPreferences_types_juniorCollege_subjects"
              options={[
                "General Paper",
                "H1 Chinese",
                "H1 Math",
                "H2 Math",
                "H3 Math",
                "H1 Physics",
                "H2 Physics",
                "H3 Physics",
                "H1 Chemistry",
                "H2 Chemistry",
                "H3 Chemistry",
                "H1 Biology",
                "H2 Biology",
                "H3 Biology",
                "H1 Economics",
                "H2 Economics",
                "H3 Economics",
                "H1 History",
                "H2 History",
                "H3 History",
                "H1 Geography",
                "H2 Geography",
                "H3 Geography",
                "H1 Literature",
                "H2 Literature",
                "H3 Literature",
                "H2 English Language Linguistics",
                "H2 Knowledge Enquiry",
                "H2 Chinese Language Literature",
                "H1 China Studies English",
                "H2 China Studies English",
                "H1 General Studies Chinese",
                "H2 China Studies Chinese",
                "H1 Malay",
                "H2 Malay",
                "H1 Chinese",
                "H2 Chinese",
                "H1 Tamil",
                "H2 Tamil",
                "H2 Accounting",
                "H2 Computing",
                "H2 Music",
                "H2 Business",
                "H1 Project Work"
              ]}
              values={[
                "generalPaper",
                "h1Chinese",
                "h1Math",
                "h2Math",
                "h3Math",
                "h1Physics",
                "h2Physics",
                "h3Physics",
                "h1Chemistry",
                "h2Chemistry",
                "h3Chemistry",
                "h1Biology",
                "h2Biology",
                "h3Biology",
                "h1Economics",
                "h2Economics",
                "h3Economics",
                "h1History",
                "h2History",
                "h3History",
                "h1Geography",
                "h2Geography",
                "h3Geography",
                "h1Literature",
                "h2Literature",
                "h3Literature",
                "h2EnglishLanguageLinguistics",
                "h2KnowledgeEnquiry",
                "h2ChineseLanguageLiterature",
                "h1ChinaStudiesEnglish",
                "h2ChinaStudiesEnglish",
                "h1GeneralStudiesChinese",
                "h2ChinaStudiesChinese",
                "h1Malay",
                "h2Malay",
                "h1Chinese",
                "h2Chinese",
                "h1Tamil",
                "h2Tamil",
                "h2Accounting",
                "h2Computing",
                "h2Music",
                "h2Business",
                "h1ProjectWork"
              ]}
              multipleSelect={true}
            />
            <TextFieldGroup
              name="tutoringPreferences_types_juniorCollege_minimumRate"
              value={types_juniorCollege_minimumRate}
              label="Minimum Rate"
              type="number"
              placeholder="Minimum Rate"
              onChange={onChange}
            />
            <CheckboxFieldGroup
              label="I can teach IP students"
              value={types_juniorCollege_teachIP}
              name="tutoringPreferences_types_juniorCollege_teachIP"
            />
          </TabContent>
          <TabContent tabID="IB" contentID="IB-c">
            <DropdownFieldGroup
              fieldTitle="Subjects"
              name="tutoringPreferences_types_IB_subjects"
              options={[
                "Mathematics HL",
                "Mathematics SL",
                "Further Mathematics HL",
                "Mathematical Studies SL",
                "Chemistry HL",
                "Chemistry SL",
                "Physics HL",
                "Physics SL",
                "Biology HL",
                "Biology SL",
                "History HL",
                "History SL",
                "Business Management HL",
                "Business Management SL",
                "Geography HL",
                "Geography SL",
                "Economics HL",
                "Economics SL",
                "Design Technology HL",
                "Design Technology SL",
                "IGTS HL",
                "IGTS SL",
                "Philosophy HL",
                "Philosophy SL",
                "Psychology HL",
                "Psychology SL",
                "English Language & Literature HL",
                "English Language & Literature SL",
                "English Literature HL",
                "English Literature SL",
                "English B HL",
                "English B SL",
                "English Ab Initio SL",
                "Chinese Language & Literature HL",
                "Chinese Language & Literature SL",
                "Chinese B HL",
                "Chinese B SL",
                "Chinese Ab Initio SL",
                "Malay B HL",
                "Malay B SL",
                "Malay Ab Initio SL",
                "Tamil B HL",
                "Tamil B SL",
                "Tamil Ab Initio SL",
                "French B HL",
                "French B SL",
                "French Ab Initio SL",
                "Spanish B HL",
                "Spanish B SL",
                "Spanish Ab Initio SL",
                "Hindi B HL",
                "Hindi B SL",
                "Hindi Ab Initio SL",
                "Theory Of Knowledge",
                "Extended Essay"
              ]}
              values={[
                "mathematicsHL",
                "mathematicsSL",
                "furtherMathematicsHL",
                "mathematicalStudiesSL",
                "chemistryHL",
                "chemistrySL",
                "physicsHL",
                "physicsSL",
                "biologyHL",
                "biologySL",
                "historyHL",
                "historySL",
                "businessManagementHL",
                "businessManagementSL",
                "geographyHL",
                "geographySL",
                "economicsHL",
                "economicsSL",
                "designTechnologyHL",
                "designTechnologySL",
                "IGTSHL",
                "IGTSSL",
                "philosophyHL",
                "philosophySL",
                "psychologyHL",
                "psychologySL",
                "englishLangLitHL",
                "englishLangLitSL",
                "englishLitHL",
                "englishLitSL",
                "englishBHL",
                "englishBSL",
                "englishAbInitioSL",
                "chineseLangLitHL",
                "chineseLangLitSL",
                "chineseBHL",
                "chineseBSL",
                "chineseAbInitioSL",
                "malayBHL",
                "malayBSL",
                "malayAbInitioSL",
                "tamilBHL",
                "tamilBSL",
                "tamilAbInitioSL",
                "frenchBHL",
                "frenchBSL",
                "frenchAbInitioSL",
                "spanishBHL",
                "spanishBSL",
                "spanishAbInitioSL",
                "hindiBHL",
                "hindiBSL",
                "hindiAbInitioSL",
                "theoryOfKnowledge",
                "extendedEssay",
                ""
              ]}
              multipleSelect={true}
            />
            <TextFieldGroup
              name="tutoringPreferences_types_IB_minimumRate"
              value={types_IB_minimumRate}
              label="Minimum Rate"
              type="number"
              placeholder="Minimum Rate"
              onChange={onChange}
            />
          </TabContent>
        </TabContentDiv>
        <div>
          <label>Experience in Teaching Students with Special Needs</label>
          <CheckboxFieldGroup label="Dyslexia" value="dyslexia" />
          <CheckboxFieldGroup
            label="Autism Spectrum Disorder (ASD)"
            value="autism"
          />
          <CheckboxFieldGroup
            label="Attention Deficit Hyperactivity Disorder (ADHD)"
            value="ADHD"
          />
          <CheckboxFieldGroup
            label="Anger Management"
            value="angerManagement"
          />
          <CheckboxFieldGroup label="Slow Learner" value="slowLearner" />
          <CheckboxFieldGroup label="Down Syndrome" value="downSyndrome" />
          <TextAreaFieldGroup
            name="tutoringPreferences_experienceInSpecialNeeds_experience"
            value={specialNeeds_experience}
            label="Please describe your experience with Special Needs"
            type="text"
            placeholder="Write your experience here"
            onChange={onChange}
          />
        </div>
        <div>
          <label>Preferred Tutoring Locations</label>
          <CheckboxFieldGroup
            label="North (Woodlands, Sembawang, Admiralty, Yishun, Khatib)"
            value="north"
          />
          <CheckboxFieldGroup
            label="North West (Bukit Batok, CCK, Bukit Panjang, Yew tee)"
            value="northWest"
          />
          <CheckboxFieldGroup
            label="West (Jurong West, Clementi, Buona Vista, Kent Ridge, Commonwealth, Queenstown)"
            value="west"
          />
          <CheckboxFieldGroup
            label="Central (Redhill, Tiong Bahru, Macpherson, Bugis, Orchard, Newton)"
            value="central"
          />
          <CheckboxFieldGroup
            label="North East (Yio Chu Kang, Potong Pasir, Punggol, Hougang, Sengkang, Bishan, AMK, Toa Payoh)"
            value="northEast"
          />
          <CheckboxFieldGroup
            label="East (Aljunied, Paya Lebar, Eunos, Tampines, Marine Parade, Pasir Ris, Changi)"
            value="east"
          />
          <CheckboxFieldGroup
            label="South (Tanjong Pagar, Marina South, Harbourfront, Telok Blangah)"
            value="south"
          />
        </div>
      </div>
    </div>
  );
};

TutoringPreferences.propTypes = {
  onChange: PropTypes.func.isRequired,
  onClick: PropTypes.func.isRequired,
  types_preSchool_subjects: PropTypes.array.isRequired,
  types_preSchool_minimumRate: PropTypes.number.isRequired,
  types_primary_subjects: PropTypes.array.isRequired,
  types_primary_minimumRate: PropTypes.number.isRequired,
  types_lowerSecondary_subjects: PropTypes.array.isRequired,
  types_lowerSecondary_minimumRate: PropTypes.number.isRequired,
  types_lowerSecondary_teachIP: PropTypes.bool.isRequired,
  types_upperSecondary_subjects: PropTypes.array.isRequired,
  types_upperSecondary_minimumRate: PropTypes.number.isRequired,
  types_upperSecondary_teachIP: PropTypes.bool.isRequired,
  types_juniorCollege_subjects: PropTypes.array.isRequired,
  types_juniorCollege_minimumRate: PropTypes.number.isRequired,
  types_juniorCollege_teachIP: PropTypes.bool.isRequired,
  types_IB_subjects: PropTypes.array.isRequired,
  specialNeeds_specialNeeds: PropTypes.array.isRequired,
  specialNeeds_experience: PropTypes.string.isRequired,
  preferredTutoringLocations: PropTypes.array.isRequired
};

export default TutoringPreferences;
