import React, { Component } from "react";
import { connect } from "react-redux";
import classnames from "classnames";
import PropTypes from "prop-types";
// import { createProfile } from "../../../../redux/actions/profileActions";
import TextFieldGroup from "../../../Elements/FormElements/TextFieldGroup/TextFieldGroup";
import RadioFieldGroup from "../../../Elements/FormElements/RadioFieldGroup/RadioFieldGroup";
import DropdownFieldGroup from "../../../Elements/FormElements/DropdownFieldGroup/DropdownFieldGroup";

const PersonalInformation = ({
  onChange,
  firstName,
  lastName,
  nric,
  gender,
  race,
  dateOfBirth,
  phoneNumber,
  postalCode,
  bank,
  accountNumber,
  tempProfile
}) => {
  return (
    <div className="form-group">
      <h2>Personal Information</h2>
      <TextFieldGroup
        label="First Name"
        type="text"
        name="personalInformation_name_firstName"
        placeholder="First Name"
        value={tempProfile.personalInformation_name_firstName}
        onChange={onChange}
      />

      <TextFieldGroup
        label="Last Name"
        type="text"
        name="personalInformation_name_lastName"
        placeholder="Last Name"
        value={tempProfile.personalInformation_name_lastName}
        onChange={onChange}
      />
      <TextFieldGroup
        label="NRIC"
        type="text"
        name="personalInformation_nric"
        placeholder="NRIC"
        value={tempProfile.personalInformation_nric}
        onChange={onChange}
      />

      <RadioFieldGroup
        fieldName="Gender"
        labels={["Male", "Female"]}
        name="personalInformation_gender"
        onChange={onChange}
        values={["male", "female"]}
      />

      <DropdownFieldGroup
        fieldTitle="Race"
        name="personalInformation_race"
        options={["Chinese", "Indian", "Malay", "Other"]}
        values={["chinese", "indian", "malay", "other"]}
        onChange={onChange}
      />

      <div className="form-group">
        <label htmlFor="date-of-birth">Date Of Birth</label>
        <input type="date" />
      </div>

      <TextFieldGroup
        label="Phone Number"
        type="number"
        name="personalInformation_phoneNumber"
        placeholder="Phone Number"
        value={phoneNumber}
        onChange={onChange}
      />

      <TextFieldGroup
        label="Postal Code"
        type="number"
        name="personalInformation_postalCode"
        placeholder="Postal Code"
        value={postalCode}
        onChange={onChange}
      />

      <TextFieldGroup
        label="Bank"
        type="text"
        name="personalInformation_bank"
        placeholder="Bank"
        value={bank}
        onChange={onChange}
      />

      <TextFieldGroup
        label="Account Number"
        type="number"
        name="personalInformation_accountNumber"
        placeholder="Account Number"
        value={accountNumber}
        onChange={onChange}
      />
    </div>
  );
};

PersonalInformation.propTypes = {
  onChange: PropTypes.func.isRequired,
  onClick: PropTypes.func.isRequired,
  firstName: PropTypes.string.isRequired,
  lastName: PropTypes.string.isRequired,
  nric: PropTypes.string.isRequired,
  gender: PropTypes.string.isRequired,
  race: PropTypes.string.isRequired,
  dateOfBirth: PropTypes.string.isRequired,
  phoneNumber: PropTypes.string.isRequired,
  postalCode: PropTypes.string.isRequired,
  bank: PropTypes.string.isRequired,
  accountNumber: PropTypes.string.isRequired
};

const mapStateToProps = state => {
  tempProfile: state.tempProfile;
};

export default connect(mapStateToProps)(PersonalInformation);
