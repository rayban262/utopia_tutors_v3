import React from "react";
import DropdownFieldGroup from "../../../Elements/FormElements/DropdownFieldGroup/DropdownFieldGroup";
import TabContent, { TabContentDiv } from "../../../Elements/Tabs/TabContent";
import TabTitle from "../../../Elements/Tabs/TabTitle";
import TextFieldGroup from "../../../Elements/FormElements/TextFieldGroup/TextFieldGroup";

/* Initialised Constants
Constants initialised are:
1. Options
2. Values
3. Subjects
4. Subject Values
*/
// Display Options for Teaching Credentials/Education Level Dropdown
const teachingCredentials_educationLevel_Options = [
  "N Level",
  "O Level",
  "Poly Student",
  "Poly Graduate",
  "A Level Student",
  "A Level Graduate",
  "Undergraduate",
  "Graduate",
  "Master's Degree",
  "Doctorate"
];
// Values to be stored for Teaching Credentials/Education Level Dropdown
const teachingCredentials_educationLevel_Values = [
  "nLevel",
  "oLevel",
  "polyStudent",
  "polyGraduate",
  "aLevelStudent",
  "aLevelGraduate",
  "undergraduate",
  "graduate",
  "mastersDegree",
  "PhD"
];

// Display Options for Teaching Credentials/Tutor Category Dropdown
const teachingCredentials_tutorCategory_Options = [
  "Part-Time JC",
  "Part-TimePoly",
  "Part-Time Undergraduate",
  "Part-Time Graduate",
  "Full-Time Tutor",
  "Ex-MOE Primary Teacher",
  "Ex-MOE Secondary Teacher",
  "Ex-MOE Junior College Teacher",
  "Current MOE Primary Teacher",
  "Current MOE Secondary Teacher",
  "Current MOE Junior College Teacher",
  "Nursery Teacher",
  "Kindergarten Teacher",
  "Polytechnic Teacher",
  "University Professor",
  "IB Teacher",
  "IGCSE Teacher",
  "NIE Trainee: Primary/Secondary",
  "NIE Trainee: Junior College"
];
// Values to be stored for Teaching Credentials/Tutor Category Dropdown
const teachingCredentials_tutorCategory_Values = [
  "partTimeJC",
  "partTimePoly",
  "partTimeUndergraduate",
  "partTimeGraduate",
  "fullTimeTutor",
  "Ex-MOEPri",
  "Ex--MOESec",
  "Ex-MOEJC",
  "currentMOEPri",
  "currentMOESec",
  "currentMOEJC",
  "nurseryTeacher",
  "kindergartenTeacher",
  "polyTeacher",
  "uniTeacher",
  "IBTeacher",
  "IGCSETeacher",
  "NIETraineePriSec",
  "NIETraineeJC"
];

// PSLE Subjects
const psle_subjects = [
  "English",
  "Math",
  "Science",
  "Chinese",
  "Higher Chinese",
  "Tamil",
  "Higher Tamil",
  "Malay",
  "Higher Malay",
  "Hindi",
  "Phonics",
  "Creative Writing",
  "Art"
];
// PSLE Subjects Values
const psle_subjects_values = [
  "english",
  "math",
  "science",
  "chinese",
  "higherChinese",
  "tamil",
  "higherTamil",
  "malay",
  "higherMalay",
  "hindi",
  "phonics",
  "creativeWriting",
  "art"
];
// oLevel Subjects
const oLevel_subjects = [
  "English",
  "Elementary Mathematics",
  "Additional Mathematics",
  "Pure Physics",
  "Pure Chemistry",
  "Pure Biology",
  "Combined Science Physics Chemistry",
  "Combined Science Biology Chemistry",
  "Combined Science Physics Biology",
  "Chinese",
  "Higher Chinese",
  "Tamil",
  "Higher Tamil",
  "Malay",
  "Higher Malay",
  "Hindi",
  "Pure History",
  "Pure Geography",
  "Combined Humanities: History, Social Studies",
  "Combined Humanities: Geography, Social Studies",
  "Social Studies",
  "Literature",
  "Home Economics"
];
// O Level Subject Values
const oLevel_subjects_values = [
  "english",
  "elementaryMath",
  "additionalMath",
  "purePhysics",
  "pureChemistry",
  "pureBiology",
  "combinedSciencePC",
  "combinedScienceBC",
  "combinedSciencePB",
  "chinese",
  "higherChinese",
  "tamil",
  "higherTamil",
  "malay",
  "higherMalay",
  "hindi",
  "pureHistory",
  "pureGeography",
  "combinedHumanitiesHSS",
  "combinedHumanitiesGSS",
  "socialSciences",
  "literature",
  "homeEconomics",
  ""
];

// A Level Subjects
const aLevel_subjects = [
  "General Paper",
  "H1 Chinese",
  "H1 Math",
  "H2 Math",
  "H3 Math",
  "H1 Physics",
  "H2 Physics",
  "H3 Physics",
  "H1 Chemistry",
  "H2 Chemistry",
  "H3 Chemistry",
  "H1 Biology",
  "H2 Biology",
  "H3 Biology",
  "H1 Economics",
  "H2 Economics",
  "H3 Economics",
  "H1 History",
  "H2 History",
  "H3 History",
  "H1 Geography",
  "H2 Geography",
  "H3 Geography",
  "H1 Literature",
  "H2 Literature",
  "H3 Literature",
  "H2 English Language Linguistics",
  "H2 Knowledge Enquiry",
  "H2 Chinese Language Literature",
  "H1 China Studies English",
  "H2 China Studies English",
  "H1 General Studies Chinese",
  "H2 China Studies Chinese",
  "H1 Malay",
  "H2 Malay",
  "H1 Chinese",
  "H2 Chinese",
  "H1 Tamil",
  "H2 Tamil",
  "H2 Accounting",
  "H2 Computing",
  "H2 Music",
  "H2 Business",
  "H1 Project Work"
];

// A Level Subject Values
const aLevel_subjects_values = [
  "generalPaper",
  "h1Chinese",
  "h1Math",
  "h2Math",
  "h3Math",
  "h1Physics",
  "h2Physics",
  "h3Physics",
  "h1Chemistry",
  "h2Chemistry",
  "h3Chemistry",
  "h1Biology",
  "h2Biology",
  "h3Biology",
  "h1Economics",
  "h2Economics",
  "h3Economics",
  "h1History",
  "h2History",
  "h3History",
  "h1Geography",
  "h2Geography",
  "h3Geography",
  "h1Literature",
  "h2Literature",
  "h3Literature",
  "h2EnglishLanguageLinguistics",
  "h2KnowledgeEnquiry",
  "h2ChineseLanguageLiterature",
  "h1ChinaStudiesEnglish",
  "h2ChinaStudiesEnglish",
  "h1GeneralStudiesChinese",
  "h2ChinaStudiesChinese",
  "h1Malay",
  "h2Malay",
  "h1Chinese",
  "h2Chinese",
  "h1Tamil",
  "h2Tamil",
  "h2Accounting",
  "h2Computing",
  "h2Music",
  "h2Business",
  "h1ProjectWork"
];

const IB_subjects = [
  "Mathematics HL",
  "Mathematics SL",
  "Further Mathematics HL",
  "Mathematical Studies SL",
  "Chemistry HL",
  "Chemistry SL",
  "Physics HL",
  "Physics SL",
  "Biology HL",
  "Biology SL",
  "History HL",
  "History SL",
  "Business Management HL",
  "Business Management SL",
  "Geography HL",
  "Geography SL",
  "Economics HL",
  "Economics SL",
  "Design Technology HL",
  "Design Technology SL",
  "IGTS HL",
  "IGTS SL",
  "Philosophy HL",
  "Philosophy SL",
  "Psychology HL",
  "Psychology SL",
  "English Language & Literature HL",
  "English Language & Literature SL",
  "English Literature HL",
  "English Literature SL",
  "English B HL",
  "English B SL",
  "English Ab Initio SL",
  "Chinese Language & Literature HL",
  "Chinese Language & Literature SL",
  "Chinese B HL",
  "Chinese B SL",
  "Chinese Ab Initio SL",
  "Malay B HL",
  "Malay B SL",
  "Malay Ab Initio SL",
  "Tamil B HL",
  "Tamil B SL",
  "Tamil Ab Initio SL",
  "French B HL",
  "French B SL",
  "French Ab Initio SL",
  "Spanish B HL",
  "Spanish B SL",
  "Spanish Ab Initio SL",
  "Hindi B HL",
  "Hindi B SL",
  "Hindi Ab Initio SL",
  "Theory Of Knowledge",
  "Extended Essay"
];

const IB_subjects_values = [
  "mathematicsHL",
  "mathematicsSL",
  "furtherMathematicsHL",
  "mathematicalStudiesSL",
  "chemistryHL",
  "chemistrySL",
  "physicsHL",
  "physicsSL",
  "biologyHL",
  "biologySL",
  "historyHL",
  "historySL",
  "businessManagementHL",
  "businessManagementSL",
  "geographyHL",
  "geographySL",
  "economicsHL",
  "economicsSL",
  "designTechnologyHL",
  "designTechnologySL",
  "IGTSHL",
  "IGTSSL",
  "philosophyHL",
  "philosophySL",
  "psychologyHL",
  "psychologySL",
  "englishLangLitHL",
  "englishLangLitSL",
  "englishLitHL",
  "englishLitSL",
  "englishBHL",
  "englishBSL",
  "englishAbInitioSL",
  "chineseLangLitHL",
  "chineseLangLitSL",
  "chineseBHL",
  "chineseBSL",
  "chineseAbInitioSL",
  "malayBHL",
  "malayBSL",
  "malayAbInitioSL",
  "tamilBHL",
  "tamilBSL",
  "tamilAbInitioSL",
  "frenchBHL",
  "frenchBSL",
  "frenchAbInitioSL",
  "spanishBHL",
  "spanishBSL",
  "spanishAbInitioSL",
  "hindiBHL",
  "hindiBSL",
  "hindiAbInitioSL",
  "theoryOfKnowledge",
  "extendedEssay",
  ""
];

const TeachingCredentials = ({
  academicQualifications_PSLE_school,
  onChange,
  psle_subjects_state,
  academicQualifications_oLevels_school,
  oLevel_subjects_state,
  ipYear4_subjects_state,
  academicQualifications_IB_school,
  academicQualifications_ipYear4_school,
  academicQualifications_aLevels_school
}) => {
  return (
    <div className="form-group">
      <h2>Tutor Credentials</h2>
      <DropdownFieldGroup
        fieldTitle="Education Level"
        name="teachingCredentials_educationLevel"
        options={teachingCredentials_educationLevel_Options}
        values={teachingCredentials_educationLevel_Values}
      />
      <DropdownFieldGroup
        fieldTitle="Tutor Category"
        name="teachingCredentials_tutorCategory"
        options={teachingCredentials_tutorCategory_Options}
        values={teachingCredentials_tutorCategory_Values}
      />
      <div>
        <TabTitle
          tabs={[
            "PSLE",
            "O Levels",
            "IP Year 4",
            "A Levels",
            "IB",
            "IGCSE",
            "Polytechnic/University"
          ]}
          tabID={[
            "psle",
            "oLevels",
            "ipYear4",
            "aLevels",
            "ib",
            "igcse",
            "polyUni"
          ]}
          contentID={[
            "psle-c",
            "oLevels-c",
            "ipYear4-c",
            "aLevels-c",
            "ib-c",
            "igcse-c",
            "polyUni-c"
          ]}
        />
        <TabContentDiv>
          {/* PSLE */}
          <TabContent tabID="psle" contentID="psle-c" showActive={true}>
            <TextFieldGroup
              name="teachingCredentials_academicQualifications_PSLE_school"
              label="School Name"
              placeholder="School Name"
              value={academicQualifications_PSLE_school}
              onChange={onChange}
            />
            {psle_subjects.map((value, index) => {
              return (
                <DropdownFieldGroup
                  fieldTitle={value}
                  name={"psle_subjects_" + psle_subjects_values[index]}
                  options={["A*", "A", "B", "C", "D", "E", "U"]}
                  values={["a*", "b", "c", "d", "e", "u"]}
                  onChange={onChange}
                />
              );
            })}
          </TabContent>
          {/* O Levels*/}
          <TabContent tabID="oLevels" contentID="oLevels-c">
            <TextFieldGroup
              name="teachingCredentials_academicQualifications_oLevels_school"
              label="School Name"
              placeholder="School Name"
              value={academicQualifications_oLevels_school}
            />

            {oLevel_subjects.map((value, index) => {
              return (
                <DropdownFieldGroup
                  fieldTitle={value}
                  name={"oLevels_subjects_" + oLevel_subjects_values[index]}
                  options={[
                    "A1",
                    "A2",
                    "B3",
                    "B4",
                    "C5",
                    "C6",
                    "D7",
                    "E8",
                    "F9"
                  ]}
                  values={[
                    "a1",
                    "a2",
                    "b3",
                    "b4",
                    "c5",
                    "c6",
                    "d7",
                    "e8",
                    "f9"
                  ]}
                  onChange={onChange}
                />
              );
            })}
          </TabContent>
          {/*IP Year 4*/}
          <TabContent tabID="ipYear4" contentID="ipYear4-c">
            <TextFieldGroup
              name="teachingCredentials_academicQualifications_ipYear4_school"
              label="School Name"
              placeholder="School Name"
              onChange={onChange}
              value={academicQualifications_ipYear4_school}
            />

            {oLevel_subjects.map((value, index) => {
              return (
                <TextFieldGroup
                  name={ipYear4_subjects_state[index]}
                  type="number"
                  label={value}
                  onChange={onChange}
                />
              );
            })}
          </TabContent>
          {/*A Levels*/}
          <TabContent tabID="aLevels" contentID="aLevels-c">
            <TextFieldGroup
              name="teachingCredentials_academicQualifications_aLevels_school"
              label="School Name"
              placeholder="School Name"
              onChange={onChange}
              value={academicQualifications_aLevels_school}
            />

            {aLevel_subjects.map((value, index) => {
              return (
                <DropdownFieldGroup
                  fieldTitle={value}
                  name={"aLevels_subjects_" + aLevel_subjects_values[index]}
                  options={["A", "B", "C", "D", "E", "S", "U"]}
                  values={["a", "b", "c", "d", "e", "s", "u"]}
                  onChange={onChange}
                />
              );
            })}
          </TabContent>
          {/*IB */}
          <TabContent tabID="ib" contentID="ib-c">
            <TextFieldGroup
              name="teachingCredentials_academicQualifications_IB_school"
              label="School Name"
              placeholder="School Name"
              value={academicQualifications_IB_school}
            />

            {IB_subjects.map((value, index) => {
              return (
                <DropdownFieldGroup
                  fieldTitle={value}
                  name={"IB_subjects_" + IB_subjects_values[index]}
                  options={["7", "6", "5", "4", "3", "2", "1"]}
                  values={["7", "6", "5", "4", "3", "2", "1"]}
                  onChange={onChange}
                />
              );
            })}
          </TabContent>
        </TabContentDiv>
      </div>
    </div>
  );
};

export default TeachingCredentials;
