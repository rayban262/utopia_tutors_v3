import React from "react";
import classnames from "classnames";
import PropTypes from "prop-types";

/*
disabled: Disables the Text Field               //String
errors: Array of error messages to be displayed 
  as part of server-side validation             //Array
info: Info about field in small text            //String
label: Input label                              //String
name: State Variable name                       //String
onChange: onChange event handler                //Function
placeholder: Placeholder text 
  when field is empty                           //String
type: Text Input type                           //String
value: Value of text input stored in db         //String
*/

const TextFieldGroup = ({
  disabled,
  errors,
  info,
  label,
  name,
  onChange,
  placeholder,
  type,
  value
}) => {
  return (
    <div className="form-group">
      <label htmlFor={label}>{label}</label>
      <input
        type={type}
        className={classnames(
          "form-control",
          errors.map(err => {
            return {
              "is-invalid": err
            };
          })
        )}
        placeholder={placeholder}
        name={name}
        value={value}
        onChange={onChange}
        disabled={disabled}
      />
      {info && <small className="form-text text-muted">{info}</small>}

      {errors.map(err => err && <div className="invalid-feedback">{err}</div>)}
    </div>
  );
};

TextFieldGroup.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  value: PropTypes.string.isRequired,
  info: PropTypes.string,
  errors: PropTypes.array,
  type: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  disabled: PropTypes.string
};

TextFieldGroup.defaultProps = {
  type: "text",
  errors: []
};

export default TextFieldGroup;
