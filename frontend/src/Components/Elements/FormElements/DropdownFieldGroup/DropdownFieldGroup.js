import React from "react";
import classnames from "classnames";
import PropTypes from "prop-types";

const DropdownFieldGroup = ({
  fieldTitle,
  labels,
  options,
  values,
  multipleSelect,
  name,
  onChange,
  fieldValue
}) => {
  return (
    <div class="form-group">
      <label>{fieldTitle}</label>
      {/* <label for={labels}>{labels}</label> */}
      {multipleSelect ? (
        <select
          multiple
          name={name}
          value={fieldValue}
          class="form-control"
          onChange={onChange}
        >
          <option selected value="">
            Choose...
          </option>
          {options.map((option, value) => (
            <option value={values[value]}>{option}</option>
          ))}
        </select>
      ) : (
        <select name={name} class="form-control" onChange={onChange}>
          <option selected value="">
            Choose...
          </option>
          {options.map((option, value) => (
            <option value={values[value]}>{option}</option>
          ))}
        </select>
      )}
    </div>
  );
};

DropdownFieldGroup.propTypes = {
  //labels: PropTypes.array.isRequired,
  options: PropTypes.array.isRequired,
  values: PropTypes.array.isRequired
};

DropdownFieldGroup.defaultProps = {};
export default DropdownFieldGroup;
