import React from "react";
import classnames from "classnames";
import PropTypes from "prop-types";

const TextAreaFieldGroup = ({
  name,
  placeholder,
  value,
  label,
  errors,
  info,
  type,
  onChange,
  disabled
}) => {
  return (
    <div className="form-group">
      <label htmlFor={label}>{label}</label>
      <textarea
        type={type}
        className={classnames(
          "form-control",
          errors.map(err => {
            return {
              "is-invalid": err
            };
          })
        )}
        placeholder={placeholder}
        name={name}
        value={value}
        onChange={onChange}
        disabled={disabled}
      />
      {info && <small className="form-text text-muted">{info}</small>}

      {errors.map(err => err && <div className="invalid-feedback">{err}</div>)}
    </div>
  );
};

TextAreaFieldGroup.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  value: PropTypes.string.isRequired,
  info: PropTypes.string,
  errors: PropTypes.array,
  type: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  disabled: PropTypes.string
};

TextAreaFieldGroup.defaultProps = {
  type: "text",
  errors: []
};

export default TextAreaFieldGroup;
