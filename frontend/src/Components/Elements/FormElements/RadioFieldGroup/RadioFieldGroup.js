import React from "react";
import classnames from "classnames";
import PropTypes from "prop-types";

/*
RadioFieldGroup Props
inline: To make radio buttons all in a line        //Boolean
fieldName: The name of the field for radio buttons //String
name: The State variable name                      //String
values: The value of the radio button stored in db //Array
onChange: onChange event handler                   //Func
labels: Labels                                     //Array
info: Small info related to the field              //String
error: Errors                                      //String
*/
const RadioFieldGroup = ({
  error,
  fieldName,
  info,
  inline,
  labels,
  name,
  onChange,
  values
}) => {
  return (
    <div className={classnames("form-check", { "form-check-inline": inline })}>
      <label>{fieldName}</label>
      {values.map((value, index) => {
        return (
          <div>
            <input
              type="radio"
              className={classnames("form-check-input", {
                "is-invalid": error
              })}
              name={name}
              value={value}
              onChange={onChange}
            />

            <label className="form-check-label" htmlFor={labels[index]}>
              {labels[index]}
            </label>
          </div>
        );
      })}
      {info && <small className="form-text text-muted">{info}</small>}
      {error && <div className="invalid-feedback">{error}</div>}
    </div>
  );
};

RadioFieldGroup.propTypes = {
  error: PropTypes.string,
  fieldName: PropTypes.string.isRequired,
  info: PropTypes.string,
  inline: PropTypes.bool,
  labels: PropTypes.array.isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  values: PropTypes.string.isRequired
};

RadioFieldGroup.defaultProps = {
  inline: false
};

export default RadioFieldGroup;
