import React from "react";

const CheckboxFieldGroup = ({ label, value }) => {
  return (
    <div class="form-check">
      <input type="checkbox" class="form-check-input" value={value} />
      <label class="form-check-label" for={label}>
        {label}
      </label>
    </div>
  );
};

export default CheckboxFieldGroup;
