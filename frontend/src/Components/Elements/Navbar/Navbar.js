import React, { Component } from "react";
import { Link } from "react-router-dom";
import classnames from "classnames";
import PropTypes from "prop-types";
import brandLogo from "../../../assets/logo/utopia_logo.png";
import "./Navbar.css";

import { connect } from "react-redux";
import { logout_user } from "../../../redux/actions/authActions";
import { clear_current_profile } from "../../../redux/actions/profileActions";
class Navbar extends Component {
  constructor(props) {
    super(props);
    this.props.clear_current_profile;
    this.onLogoutClick = this.onLogoutClick.bind(this);
  }

  onLogoutClick(e) {
    e.preventDefault();
    this.props.logout_user();
  }

  render() {
    const { isAuthenticated, user } = this.props.auth;
    // Links visible to a logged in user
    const authLinks = (
      <ul className="navbar-nav Auth-Link ml-auto">
        <li>
          <a href="#" onClick={this.onLogoutClick} className="nav-link">
            Logout
          </a>
        </li>
      </ul>
    );
    // Links visible to a logged out user
    const guestLinks = (
      <ul className="navbar-nav Auth-Link ml-auto">
        <li>
          <Link className="nav-link" to={this.props.Login}>
            Login
          </Link>
        </li>
        <li>
          <Link className="nav-link" to={this.props.Register}>
            Register
          </Link>
        </li>
      </ul>
    );

    return (
      <nav className="navbar navbar-expand-lg sticky-top">
        <Link className="navbar-brand" to={this.props.Home}>
          <img src={brandLogo} />
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon" />
        </button>

        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav Navbar-Tabs mx-auto">
            <li className="nav-item">
              <Link className="nav-link" to={this.props.Home + "#Process"}>
                Process
              </Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to={this.props.Home + "#Why-Us"}>
                Why Us?
              </Link>
            </li>
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle Dropdown-Toggle"
                href="#"
                id="navbarDropdown"
                role="button"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                FAQs
              </a>
              <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                <a className="dropdown-item" href="#For-Tutors">
                  For Tutors
                </a>
                <a className="dropdown-item" href="#For-Tutees">
                  For Tutees
                </a>
              </div>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#About">
                About Us
              </a>
            </li>
          </ul>
          {isAuthenticated ? authLinks : guestLinks}
        </div>
      </nav>
    );
  }
}

Navbar.propTypes = {
  logout_user: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { logout_user, clear_current_profile }
)(Navbar);
