import React from "react";
import classnames from "classnames";
import PropTypes from "prop-types";
// div: If div is true, tab
// contentID: Content ID
// Content: The actual content in the tab
// tabID: The id of the tabs

export const TabContentDiv = props => {
  return <div className="tab-content">{props.children}</div>;
};

const TabContent = ({ contentID, tabID, showActive, children }) => {
  return (
    <div
      className={classnames("tab-pane fade", showActive && "show active")}
      id={contentID}
      role="tabpanel"
      aria-labelledby={tabID}
    >
      {children}
    </div>
  );
};

export default TabContent;

{
  /* <div
    class="tab-pane fade show active"
    id="home"
    role="tabpanel"
    aria-labelledby="home-tab"
  >
    ...
  </div>
  <div
    class="tab-pane fade"
    id="profile"
    role="tabpanel"
    aria-labelledby="profile-tab"
  >
    ...
  </div>
  <div
    class="tab-pane fade"
    id="contact"
    role="tabpanel"
    aria-labelledby="contact-tab"
  >
    ...
  </div> */
}
