import React from "react";
import classnames from "classnames";
import PropTypes from "prop-types";

const TabTitle = ({ tabs, tabID, contentID }) => {
  return (
    <ul className="nav nav-tabs" role="tablist">
      {tabs.map((tab, index) => {
        return (
          <li className="nav-item">
            <a
              className={classnames("nav-link", index === 0 ? "active" : "")}
              href={"#" + contentID[index]}
              id={tabID[index]}
              data-toggle="tab"
              role="tab"
              aria-controls={contentID[index]}
              aria-selected={index === 0 ? "true" : "false"}
            >
              {tab}
            </a>
          </li>
        );
      })}
    </ul>
  );
};

TabTitle.propTypes = {
  tabs: PropTypes.array.isRequired,
  tabID: PropTypes.array.isRequired,
  contentID: PropTypes.array.isRequired
};

export default TabTitle;
